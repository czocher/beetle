package org.czocher.beetle;

import com.sun.org.apache.bcel.internal.Constants;
import com.sun.org.apache.bcel.internal.generic.ClassGen;
import org.czocher.beetle.errorcontainer.DefaultErrorContainer;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.Lexer;
import org.czocher.beetle.lexer.utils.SourceReader;
import org.czocher.beetle.parser.Parser;
import org.czocher.beetle.parser.interfaces.TreeNode;
import org.czocher.beetle.parser.utils.DotRenderer;

import java.io.IOException;

public class Main {

	public static void main(final String[] args) throws IOException {
		final SourceReader sourceReader = new SourceReader(
				Main.class.getResourceAsStream("source.btl"));
		final ErrorContainer errorContainer = new DefaultErrorContainer();

		final Lexer lexer = new Lexer(sourceReader, errorContainer);

		/*System.out.println("StartLine\tStartCol\tEndLine\tEndCol\tLexem Type\t\t\t\t\tLexem");
		while (lexer.hasNextLexem()) {
			System.out.println("Current Lexem: " + lexer.currentLexem());
			final Lexem<?> lexem = lexer.nextLexem();
			System.out.print(lexem.getStartLine() + "\t\t\t"
					+ lexem.getStartCol() + "\t\t\t");
			System.out.print(lexem.getEndLine() + "\t\t" + lexem.getEndCol()
					+ "\t\t");
			System.out.print(lexem.getType() + "\t\t");
			System.out.println(lexem.toString());
		}
		System.out.println("Current Lexem: " + lexer.currentLexem());
		System.out.println(errorContainer.toString());*/

		Parser parser = new Parser(lexer, errorContainer);
		TreeNode result = parser.parse();
		result.fillType();


		parser.getSymbolScope();
		if (!errorContainer.getErrorList().isEmpty()) {
			System.out.println(errorContainer.toString());
		}
		//} else {
			DotRenderer treeRenderer = new DotRenderer(result.toString());
			treeRenderer.render();
			DotRenderer variableRenderer = new DotRenderer(parser.getSymbolScope().toString());
			variableRenderer.setGraphName("Variables");
			variableRenderer.render();
			generateCode(result);
		//}
	}

	private static void generateCode(TreeNode result) throws IOException {
		ClassGen classGen = new ClassGen("Output", "java.lang.Object", "dummy", Constants.ACC_PUBLIC | Constants.ACC_SUPER, null);


		result.generateCode(classGen, null);

		classGen.getJavaClass().dump("Output.class");

	}
}
