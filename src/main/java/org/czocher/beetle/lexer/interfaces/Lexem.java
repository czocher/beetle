package org.czocher.beetle.lexer.interfaces;

import lombok.Data;

@Data
public abstract class Lexem<T> {

    protected int startLine;

    protected int endLine;

    protected int startCol;

    protected int endCol;

    public abstract LexemType getType();

    public abstract T getValue();

}
