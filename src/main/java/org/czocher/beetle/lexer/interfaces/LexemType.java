package org.czocher.beetle.lexer.interfaces;

import lombok.Getter;
import lombok.ToString;

import java.util.HashMap;
import java.util.Map;

@ToString
public enum LexemType {
    MAIN("main"), SEMICOLON(";"), COLON(":"), VAR("var"),
    OPENING_BRACE("{"), CLOSING_BRACE("}"), OPENING_PARENTHESIS("("), CLOSING_PARENTHESIS(")"),
    IF("if"), DO("do"), BLOCK("block"), CALL("call"), ALLOC("alloc"), IDENT("IDENT"), LBOOL("LBOOL"), LINT("LINT"),
    LREAL("LREAL"), LSTRING("LSTRING"), EQ("="), SPECIAL("SPECIAL"), COMMA(","), MULTOP("MULTOP"), ADDOP("ADDOP"),
    TYPEIDENT("TYPEIDENT"), RELOP("RELOP"), PRINT("print"), READ("read"), ARRAYOF("arrayof"), SUB("sub"),
    EOF("end-of-file");

    private static final Map<String, LexemType> lookup = new HashMap<>();

    static {
        for (LexemType t : LexemType.values())
            lookup.put(t.getValue(), t);
    }
    @Getter
    private final String value;

    LexemType(String value) {
        this.value = value;
    }

    public static LexemType get(String token) {
        return lookup.get(token);
    }

}
