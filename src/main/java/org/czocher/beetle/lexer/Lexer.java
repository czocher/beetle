package org.czocher.beetle.lexer;


import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.interfaces.Lexem;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.lexer.interpreters.COMPEQLexemInterpreter;
import org.czocher.beetle.lexer.interpreters.KEYWORDLexemInterpreter;
import org.czocher.beetle.lexer.interpreters.LNUMBERLexemInterpreter;
import org.czocher.beetle.lexer.lexems.literals.LSTRINGLexem;
import org.czocher.beetle.lexer.lexems.operators.ADDOPLexem;
import org.czocher.beetle.lexer.lexems.operators.MULTOPLexem;
import org.czocher.beetle.lexer.lexems.special.EOFLexem;
import org.czocher.beetle.lexer.lexems.special.SPECIALLexem;
import org.czocher.beetle.lexer.utils.LexerUtils;
import org.czocher.beetle.lexer.utils.SourceReader;

@RequiredArgsConstructor
public class Lexer {

    private final SourceReader sourceReader;
    @Getter
    private final ErrorContainer errorContainer;
    private Lexem<?> nextLexem;
    private Lexem<?> currentLexem;

    public boolean hasNextLexem() {
        peek();
        return nextLexem != null && nextLexem.getType() != LexemType.EOF;
    }

    public <T> Lexem<T> currentLexem() {
        return (Lexem<T>) this.currentLexem;
    }

    public <T> Lexem<T> peek() {
        if (nextLexem == null) {
            boolean commentStart = false;
            char c;
            int startCol;
            int startLine;
            startCol = sourceReader.getCol();
            startLine = sourceReader.getLine();
            while (sourceReader.hasNextChar()) {
                c = sourceReader.peek();
                if (commentStart) {
                    if (sourceReader.nextChar() == '$') {
                        commentStart = false;
                    }
                } else if (c == '$') {
                    sourceReader.nextChar();
                    commentStart = true;
                } else if (Character.isDigit(c)) {
                    nextLexem = LNUMBERLexemInterpreter.interpret(sourceReader, errorContainer);
                } else if (c == '\'') {
                    nextLexem = LSTRINGLexem.interpret(sourceReader, errorContainer);
                } else if (Character.isAlphabetic(c)) {
                    nextLexem = KEYWORDLexemInterpreter.interpret(sourceReader);
                } else if (LexerUtils.isComparisonOrEqualitySign(c)) {
                    nextLexem = COMPEQLexemInterpreter.interpret(sourceReader, errorContainer);
                } else if (LexerUtils.isADDOPSign(c)) {
                    nextLexem = ADDOPLexem.interpret(sourceReader);
                } else if (LexerUtils.isMULTOPSign(c)) {
                    nextLexem = MULTOPLexem.interpret(sourceReader);
                } else if (LexerUtils.isSpecialSign(c)) {
                    nextLexem = SPECIALLexem.interpret(sourceReader);
                } else {
                    sourceReader.nextChar();
                }
                if (nextLexem != null) {
                    nextLexem.setStartCol(startCol);
                    nextLexem.setStartLine(startLine);
                    nextLexem.setEndCol(sourceReader.getCol());
                    nextLexem.setEndLine(sourceReader.getLine());
                    break;
                }
            }
            if (!sourceReader.hasNextChar() && nextLexem == null) {
                nextLexem = new EOFLexem();
                nextLexem.setStartCol(startCol);
                nextLexem.setStartLine(startLine);
                nextLexem.setEndCol(sourceReader.getCol());
                nextLexem.setEndLine(sourceReader.getLine());
            }
        }
        return (Lexem<T>) nextLexem;
    }

    public <T> Lexem<T> nextLexem() {
        peek();
        currentLexem = this.nextLexem;
        this.nextLexem = null;
        return (Lexem<T>) currentLexem;
    }

}
