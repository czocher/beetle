package org.czocher.beetle.lexer.interpreters;


import lombok.NonNull;
import org.czocher.beetle.errorcontainer.interfaces.Error;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.interfaces.Lexem;
import org.czocher.beetle.lexer.lexems.operators.EQOPLexem;
import org.czocher.beetle.lexer.lexems.operators.RELOPLexem;
import org.czocher.beetle.lexer.utils.SourceReader;
import org.czocher.beetle.lexer.utils.LexerUtils;

public class COMPEQLexemInterpreter {

    public static Lexem<?> interpret(@NonNull final SourceReader sourceReader, @NonNull ErrorContainer errorContainer) {
        int startLine = sourceReader.getLine();
        int startCol = sourceReader.getCol();
        char c;
        final StringBuilder sb = new StringBuilder();
        while (sourceReader.hasNextChar()) {
            c = sourceReader.peek();
            if (LexerUtils.isComparisonOrEqualitySign(c)) {
                sb.append(sourceReader.nextChar());
            } else {
                break;
            }
        }
        String operation = sb.toString();
        switch (operation) {
            case "=":
                return new EQOPLexem(operation);
            case "==":
            case "!=":
            case "<=":
            case ">=":
            case "<":
            case ">":
                return new RELOPLexem(operation);
            default:
                errorContainer.addError(new Error("Unknown comparison symbol.", startLine, startCol, sourceReader.getLine(), sourceReader.getCol()));
                return null;
        }

    }

}
