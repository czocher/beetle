package org.czocher.beetle.lexer.interpreters;

import lombok.NonNull;

import org.czocher.beetle.errorcontainer.interfaces.Error;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.interfaces.Lexem;
import org.czocher.beetle.lexer.lexems.literals.LINTLexem;
import org.czocher.beetle.lexer.lexems.literals.LREALLexem;
import org.czocher.beetle.lexer.utils.SourceReader;

public class LNUMBERLexemInterpreter {

	public static Lexem<?> interpret(@NonNull final SourceReader sourceReader,
			@NonNull final ErrorContainer errorContainer) {
		final int startLine = sourceReader.getLine();
		final int startCol = sourceReader.getCol();
		boolean isFloat = false;
		char c;
		final StringBuilder sb = new StringBuilder();
		while (sourceReader.hasNextChar()) {
			c = sourceReader.peek();
			if (Character.isDigit(c)) {
				sb.append(sourceReader.nextChar());
			} else if (c == '.') {
				if (isFloat) {
					errorContainer.addError(new Error("Multiple float points.",
							startLine, startCol, sourceReader.getLine(),
							sourceReader.getCol()));
					return null;
				}
				sb.append(sourceReader.nextChar());
				isFloat = true;
			} else {
				break;
			}
		}
		if (isFloat) {
			return new LREALLexem(Float.parseFloat(sb.toString()));
		} else {
			return new LINTLexem(Integer.parseInt(sb.toString()));
		}
	}
}
