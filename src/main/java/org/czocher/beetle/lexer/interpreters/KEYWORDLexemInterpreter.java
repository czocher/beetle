package org.czocher.beetle.lexer.interpreters;

import lombok.NonNull;
import org.czocher.beetle.lexer.interfaces.Lexem;
import org.czocher.beetle.lexer.lexems.keywords.*;
import org.czocher.beetle.lexer.lexems.literals.IDENTLexem;
import org.czocher.beetle.lexer.lexems.literals.LBOOLLexem;
import org.czocher.beetle.lexer.lexems.operators.ADDOPLexem;
import org.czocher.beetle.lexer.lexems.operators.MULTOPLexem;
import org.czocher.beetle.lexer.utils.SourceReader;

public class KEYWORDLexemInterpreter {

    public static Lexem<?> interpret(@NonNull final SourceReader sourceReader) {
        final IDENTLexem ident = IDENTLexem.interpret(sourceReader);
        Lexem lexem;

        switch (ident.getValue()) {
            case "true":
                lexem = new LBOOLLexem(true);
                break;
            case "false":
                lexem = new LBOOLLexem(false);
                break;
            case "main":
                lexem = new MAINLexem();
                break;
            case "var":
                lexem = new VARLexem();
                break;
            case "arrayof":
                lexem = new ARRAYOFLexem();
                break;
            case "integer":
            case "float":
            case "boolean":
            case "string":
                lexem = new TYPEIDENTLexem(ident.getValue());
                break;
            case "and":
                lexem = new MULTOPLexem("and");
                break;
            case "or":
                lexem = new ADDOPLexem("or");
                break;
            case "print":
                lexem = new OUTLexem();
                break;
            case "read":
                lexem = new INLexem();
                break;
            case "if":
                lexem = new IFLexem();
                break;
            case "do":
                lexem = new DOLexem();
                break;
            case "block":
                lexem = new BLOCKLexem();
                break;
            case "call":
                lexem = new CALLLexem();
                break;
            case "alloc":
                lexem = new ALLOCLexem();
                break;
            case "sub":
                lexem = new SUBLexem();
                break;
            default:
                lexem = ident;
                break;
        }

        return lexem;
    }

}
