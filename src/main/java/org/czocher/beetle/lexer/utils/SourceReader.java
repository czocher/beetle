package org.czocher.beetle.lexer.utils;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.io.*;

public class SourceReader {

    @Getter
    @Setter
    private int line;
    @Getter
    @Setter
    private int col;
    private BufferedReader bufferedReader;
    private int peek = -1;

    public SourceReader(@NonNull String fileName) throws FileNotFoundException {
        bufferedReader = new BufferedReader(new FileReader(new File(fileName)));
    }

    public SourceReader(@NonNull InputStream sourceStream) {
        bufferedReader = new BufferedReader(new InputStreamReader(sourceStream));
    }

    public boolean hasNextChar() {
        peek();
        return peek != -1;
    }

    public char nextChar() {
        if (peek == -1) {
            peek();
        }
        char c = (char) peek;
        peek = -1;
        if (c == '\n') {
            col = 0;
            line++;
        } else {
            col++;
        }
        return c;
    }

    public char peek() {
        if (peek == -1) {
            try {
                peek = bufferedReader.read();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return (char) peek;
    }


}
