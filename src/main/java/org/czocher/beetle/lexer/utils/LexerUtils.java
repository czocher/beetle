package org.czocher.beetle.lexer.utils;

public class LexerUtils {

    public static boolean isComparisonOrEqualitySign(final char c) {
        switch (c) {
            case '=':
                return true;
            case '!':
                return true;
            case '<':
                return true;
            case '>':
                return true;
            default:
                return false;
        }
    }

    public static boolean isSpecialSign(final char c) {
        switch (c) {
            case '{':
                return true;
            case '}':
                return true;
            case '(':
                return true;
            case ')':
                return true;
            case ';':
                return true;
            case ':':
                return true;
            case ',':
                return true;
            default:
                return false;
        }
    }

    public static boolean isADDOPSign(char c) {
        switch (c) {
            case '+':
                return true;
            case '-':
                return true;
            default:
                return false;
        }
    }

    public static boolean isMULTOPSign(char c) {
        switch (c) {
            case '*':
                return true;
            case '/':
                return true;
            default:
                return false;
        }
    }
}
