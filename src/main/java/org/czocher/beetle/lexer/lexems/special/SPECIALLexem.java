package org.czocher.beetle.lexer.lexems.special;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import org.czocher.beetle.lexer.interfaces.Lexem;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.lexer.utils.SourceReader;
import org.czocher.beetle.lexer.utils.LexerUtils;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class SPECIALLexem extends Lexem<String> {

    private final String value;

    public static SPECIALLexem interpret(@NonNull final SourceReader sourceReader) {
        char c;
        final StringBuilder sb = new StringBuilder();
        while (sourceReader.hasNextChar()) {
            c = sourceReader.peek();
            if (LexerUtils.isSpecialSign(c)) {
                sb.append(sourceReader.nextChar());
                break;
            }
        }
        return new SPECIALLexem(sb.toString());
    }

    @Override
    public LexemType getType() {
        switch (value) {
            case "{":
                return LexemType.OPENING_BRACE;
            case "}":
                return LexemType.CLOSING_BRACE;
            case "(":
                return LexemType.OPENING_PARENTHESIS;
            case ")":
                return LexemType.CLOSING_PARENTHESIS;
            case ":":
                return LexemType.COLON;
            case ";":
                return LexemType.SEMICOLON;
            case ",":
                return LexemType.COMMA;
            default:
                return LexemType.SPECIAL;
        }
    }

}
