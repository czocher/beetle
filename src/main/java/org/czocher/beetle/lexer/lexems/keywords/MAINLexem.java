package org.czocher.beetle.lexer.lexems.keywords;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.czocher.beetle.lexer.interfaces.Lexem;
import org.czocher.beetle.lexer.interfaces.LexemType;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class MAINLexem extends Lexem<String> {

    @Override
    public LexemType getType() {
        return LexemType.MAIN;
    }

    @Override
    public String getValue() {
        return "main";
    }

}
