package org.czocher.beetle.lexer.lexems.literals;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.czocher.beetle.lexer.interfaces.Lexem;
import org.czocher.beetle.lexer.interfaces.LexemType;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class LINTLexem extends Lexem<Integer> {

    private final int value;

    @Override
    public LexemType getType() {
        return LexemType.LINT;
    }

    public Integer getValue() {
        return value;
    }
}
