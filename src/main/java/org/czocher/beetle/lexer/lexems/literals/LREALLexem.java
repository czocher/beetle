package org.czocher.beetle.lexer.lexems.literals;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.czocher.beetle.lexer.interfaces.Lexem;
import org.czocher.beetle.lexer.interfaces.LexemType;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class LREALLexem extends Lexem<Float> {

    private final float value;

    @Override
    public LexemType getType() {
        return LexemType.LREAL;
    }

    public Float getValue() {
        return value;
    }
}
