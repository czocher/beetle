package org.czocher.beetle.lexer.lexems.special;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.czocher.beetle.lexer.interfaces.Lexem;
import org.czocher.beetle.lexer.interfaces.LexemType;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class EOFLexem extends Lexem<String> {

    @Override
    public String getValue() {
        return "eof";
    }

    @Override
    public LexemType getType() {
        return LexemType.EOF;
    }

}
