package org.czocher.beetle.lexer.lexems.operators;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import org.czocher.beetle.lexer.interfaces.Lexem;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.lexer.utils.SourceReader;
import org.czocher.beetle.lexer.utils.LexerUtils;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class ADDOPLexem extends Lexem<String> {

    private String value;

    public static ADDOPLexem interpret(@NonNull final SourceReader sourceReader) {
        char c;
        final StringBuilder sb = new StringBuilder();
        while (sourceReader.hasNextChar()) {
            c = sourceReader.peek();
            if (LexerUtils.isADDOPSign(c)) {
                sb.append(sourceReader.nextChar());
                break;
            }
        }
        return new ADDOPLexem(sb.toString());
    }

    @Override
    public LexemType getType() {
        return LexemType.ADDOP;
    }

}
