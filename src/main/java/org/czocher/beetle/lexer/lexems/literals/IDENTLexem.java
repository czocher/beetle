package org.czocher.beetle.lexer.lexems.literals;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import org.czocher.beetle.lexer.interfaces.Lexem;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.lexer.utils.SourceReader;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class IDENTLexem extends Lexem<String> {

    private String value;

    public static IDENTLexem interpret(@NonNull final SourceReader sourceReader) {
        boolean beginning = true;
        char c;
        final StringBuilder sb = new StringBuilder();
        while (sourceReader.hasNextChar()) {
            c = sourceReader.peek();
            if (beginning && Character.isAlphabetic(c)) {
                sb.append(sourceReader.nextChar());
                beginning = false;
            } else if (!beginning && (Character.isLetterOrDigit(c) || c == '_')) {
                sb.append(sourceReader.nextChar());
            } else {
                break;
            }
        }
        return new IDENTLexem(sb.toString());
    }

    @Override
    public LexemType getType() {
        return LexemType.IDENT;
    }
}
