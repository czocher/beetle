package org.czocher.beetle.lexer.lexems.keywords;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.czocher.beetle.lexer.interfaces.Lexem;
import org.czocher.beetle.lexer.interfaces.LexemType;

import javax.lang.model.type.NullType;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class CALLLexem extends Lexem<NullType> {

    @Override
    public LexemType getType() {
        return LexemType.CALL;
    }

    @Override
    public NullType getValue() {
        return null;
    }

}
