package org.czocher.beetle.lexer.lexems.operators;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.czocher.beetle.lexer.interfaces.Lexem;
import org.czocher.beetle.lexer.interfaces.LexemType;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class EQOPLexem extends Lexem<String> {

    private String value;

    @Override
    public LexemType getType() {
        return LexemType.EQ;

    }
}
