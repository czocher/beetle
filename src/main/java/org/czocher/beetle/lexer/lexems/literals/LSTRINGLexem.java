package org.czocher.beetle.lexer.lexems.literals;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import org.czocher.beetle.errorcontainer.interfaces.Error;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.interfaces.Lexem;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.lexer.utils.SourceReader;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class LSTRINGLexem extends Lexem<String> {

    private String value;

    public static LSTRINGLexem interpret(@NonNull final SourceReader sourceReader, @NonNull ErrorContainer errorContainer) {
        int startLine = sourceReader.getLine();
        int startCol = sourceReader.getCol();
        boolean opened = false;
        char c;
        final StringBuilder sb = new StringBuilder();
        while (sourceReader.hasNextChar()) {
            c = sourceReader.nextChar();
            if (c == '\'') {
                if (opened) {
                    opened = false;
                    break;
                }
                opened = true;
            } else if (opened) {
                sb.append(c);
            } else {
                break;
            }
        }
        if (opened) {
            errorContainer.addError(new Error("String literal not closed.", startLine, startCol, sourceReader.getLine(), sourceReader.getCol()));
        }
        return new LSTRINGLexem(sb.toString());
    }

    @Override
    public LexemType getType() {
        return LexemType.LSTRING;
    }
}
