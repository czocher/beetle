package org.czocher.beetle.lexer.lexems.literals;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import org.czocher.beetle.lexer.interfaces.Lexem;
import org.czocher.beetle.lexer.interfaces.LexemType;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class LBOOLLexem extends Lexem<Boolean> {

	private final boolean value;

	@Override
	public LexemType getType() {
		return LexemType.LBOOL;
	}

	@Override
	public Boolean getValue() {
		return value;
	}

}
