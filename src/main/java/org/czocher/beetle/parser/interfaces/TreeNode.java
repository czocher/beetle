package org.czocher.beetle.parser.interfaces;

import com.sun.org.apache.bcel.internal.generic.ClassGen;
import com.sun.org.apache.bcel.internal.generic.MethodGen;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.apache.commons.lang3.StringEscapeUtils;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.Lexer;
import org.czocher.beetle.lexer.interfaces.Lexem;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.*;


public abstract class TreeNode {

    protected final Logger log;

    protected final Lexer lexer;
    protected final ErrorContainer errorContainer;
    @Getter
    protected SymbolScope symbolScope;
    @Getter
    @Setter
    protected Set<LexemType> superFirstSet;
    @Getter
    @Setter
    protected List<TreeNode> children;
    @Getter
    @Setter
    protected Set<LexemType> firstSet;
    @Getter
    @Setter
    protected Set<LexemType> followSet;
    @Getter
    @Setter
    protected SemanticType type;
    @Getter
    @Setter
    protected int startLine;
    @Getter
    @Setter
    protected int startCol;
    @Getter
    @Setter
    protected int endLine;
    @Getter
    @Setter
    protected int endCol;

    public TreeNode(@NonNull Lexer lexer, @NonNull SymbolScope symbolScope,
                    @NonNull ErrorContainer errorContainer) {
        this(lexer, symbolScope, errorContainer, Collections.<LexemType>emptySet());
    }

    public TreeNode(@NonNull Lexer lexer, @NonNull SymbolScope symbolScope, @NonNull ErrorContainer errorContainer,
                    @NonNull Set<LexemType> superFirstSet) {
        setType(SemanticType.UNKNOWN);
        this.firstSet = new HashSet<>();
        this.followSet = new HashSet<>();
        this.superFirstSet = superFirstSet;
        this.followSet.addAll(superFirstSet);
        this.children = new ArrayList<>();
        this.lexer = lexer;
        this.errorContainer = errorContainer;
        this.symbolScope = symbolScope;
        log = LoggerFactory.getLogger(this.getClass().getSimpleName());
    }

    public TreeNode parse() {
        if (!preParse()) {
            return null;
        }
        fillLocation();
        return postParse();
    }

    private void fillLocation() {
        startLine = lexer.peek().getStartLine();
        startCol = lexer.peek().getStartCol();
        endLine = lexer.peek().getEndLine();
        endCol = lexer.peek().getEndCol();
    }

    public boolean preParse() {
        if (!firstSet.contains(lexer.peek().getType())) {
            return false;
        }
        log.debug("Found {}", this.getClass().getSimpleName());
        return true;
    }

    public abstract TreeNode postParse();

    protected void skip() {
        log.warn("Skipping to one of: {}.", followSet);
        while (lexer.hasNextLexem()) {
            Lexem<?> lexem = lexer.peek();
            if (followSet.contains(lexem.getType())) {
                return;
            } else {
                log.error("Follow set doesn't contain {}.", lexem);
            }
            lexer.nextLexem();
        }
    }

    protected boolean acceptOrSkip(@NonNull LexemType... expectedLexems) {
        if (!accept(expectedLexems)) {
            skip();
            return false;
        }
        return true;
    }

    protected boolean accept(@NonNull LexemType... expectedLexems) {
        Lexem next = lexer.peek();
        for (LexemType expectedLexemType : expectedLexems) {
            if (next.getType() == expectedLexemType) {
                lexer.nextLexem();
                log.debug("Accepted " + next.getType());
                return true;
            }
        }

        log.error("Line: {}, Column: {}. Expected one of: {}. Found: {}.", next.getStartLine(), next.getStartCol(),
                expectedLexems, next.getType());
        errorContainer.addError("Expected " + Arrays.toString(expectedLexems) + " found " + next + ".",
                next.getStartLine(), next.getStartCol(), next.getEndLine(), next.getEndCol());
        return false;
    }

    public boolean hasChildren() {
        return children.isEmpty();
    }

    public void addChild(@NonNull TreeNode child) {
        children.add(child);
    }

    public void addChild(int index, @NonNull TreeNode child) {
        children.add(index, child);
    }

    public void addChildren(@NonNull List<TreeNode> children) {
        this.children.addAll(children);
    }

    public void removeChild(int index) {
        children.remove(index);
    }

    public TreeNode getChild(int index) {
        return children.get(index);
    }

    public void fillType() {
        for (TreeNode child : children) {
            child.fillType();
        }
        if (getType() == SemanticType.UNKNOWN) {
            if (children.size() == 1) {
                setType(children.get(0).getType());
            } else if (children.size() == 2) {
                if (getChild(0).getType() == getChild(0).getType()) {
                    setType(getChild(0).getType());
                } else {
                    errorContainer.addError("Unequal types", startLine, startCol, endLine, endCol);
                }
            }
        }
    }

    public void generateCode(ClassGen cg, MethodGen mg) {
        for (TreeNode child : children) {
            child.generateCode(cg, mg);
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        Field[] fields = this.getClass().getDeclaredFields();

        StringBuilder valueBuilder = new StringBuilder();
        for (Field field : fields) {
            if (!field.getName().equals("log") && !field.getName().equals("subScope")) {
                try {
                    field.setAccessible(true);
                    String val = StringEscapeUtils.escapeXml11(String.valueOf(field.get(this)));
                    valueBuilder.append(field.getName()).append("=").append(val).append("|");
                    field.setAccessible(false);
                } catch (IllegalAccessException e) {
                    // Ignore
                }
            }
        }
        if (valueBuilder.length() != 0) {
            valueBuilder.delete(valueBuilder.lastIndexOf("|"), valueBuilder.lastIndexOf("|"));
        }
        final String nodeType = this.getClass().getSimpleName();
        final String nodeDescription = "%s[shape=record, label=\"%s|{%stype=%s}\"]\n";
        final String nodeConnection = "%s->%s\n";
        final String uniqueAddress = Integer.toString(System.identityHashCode(this));
        final StringBuilder type = new StringBuilder();
        type.append(this.type);

        sb.append(String.format(nodeDescription, uniqueAddress, nodeType, valueBuilder.toString(), type));
        for (TreeNode node : children) {
            sb.append(String.format(nodeConnection, uniqueAddress, System.identityHashCode(node)));
            sb.append(node.toString());
        }

        return sb.toString();
    }
}
