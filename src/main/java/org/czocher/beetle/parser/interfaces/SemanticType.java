package org.czocher.beetle.parser.interfaces;

import com.sun.org.apache.bcel.internal.generic.ArrayType;
import com.sun.org.apache.bcel.internal.generic.Type;
import org.czocher.beetle.lexer.interfaces.Lexem;

public enum SemanticType {
    UNKNOWN, VOID, INTEGER, BOOLEAN, FLOAT, STRING, ARRAY_INTEGER, ARRAY_BOOLEAN, ARRAY_FLOAT, ARRAY_STRING;

    public static SemanticType fromLiteralLexem(Lexem lexem) {
        switch (lexem.getType()) {
            case LINT:
                return SemanticType.INTEGER;
            case LREAL:
                return SemanticType.FLOAT;
            case LSTRING:
                return SemanticType.STRING;
            case LBOOL:
                return SemanticType.BOOLEAN;
            default:
                return UNKNOWN;
        }
    }

    public static SemanticType fromTYPEIDENTLexem(Lexem<String> lexem) {
        switch (lexem.getValue()) {
            case "integer":
                return SemanticType.INTEGER;
            case "float":
                return SemanticType.FLOAT;
            case "string":
                return SemanticType.STRING;
            case "boolean":
                return SemanticType.BOOLEAN;
            default:
                return UNKNOWN;
        }
    }

    public boolean isArrayType() {
        switch (this) {
            case ARRAY_BOOLEAN:
            case ARRAY_STRING:
            case ARRAY_INTEGER:
            case ARRAY_FLOAT:
                return true;
            default:
                return false;
        }
    }

    public Type javaType() {
        switch (this) {
            case VOID:
                return Type.VOID;
            case INTEGER:
                return Type.INT;
            case STRING:
                return Type.STRING;
            case BOOLEAN:
                return Type.BOOLEAN;
            case FLOAT:
                return Type.FLOAT;
            case ARRAY_INTEGER:
                return new ArrayType(Type.INT, 1);
            case ARRAY_FLOAT:
                return new ArrayType(Type.FLOAT, 1);
            case ARRAY_BOOLEAN:
                return new ArrayType(Type.BOOLEAN, 1);
            case ARRAY_STRING:
                return new ArrayType(Type.STRING, 1);
            default:
                return Type.VOID;
        }
    }

    public Type javaSubType() {
        switch (this) {
            case VOID:
                return Type.VOID;
            case INTEGER:
            case ARRAY_INTEGER:
                return Type.INT;
            case STRING:
            case ARRAY_STRING:
                return Type.STRING;
            case BOOLEAN:
            case ARRAY_BOOLEAN:
                return Type.BOOLEAN;
            case FLOAT:
            case ARRAY_FLOAT:
                return Type.FLOAT;
            default:
                return Type.VOID;
        }
    }
}

