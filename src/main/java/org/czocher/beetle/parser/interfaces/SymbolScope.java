package org.czocher.beetle.parser.interfaces;

import lombok.NonNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SymbolScope {

    private SymbolScope superScope;
    private List<SymbolScope> subScopes;
    private Map<String, Symbol> symbols;

    public SymbolScope(SymbolScope superScope) {
        this();
        this.superScope = superScope;
    }

    public SymbolScope() {
        this.symbols = new HashMap<>();
        this.subScopes = new ArrayList<>();
        this.superScope = null;
    }

    public void addSymbol(@NonNull Symbol symbol) {
        this.symbols.put(symbol.getIdentificator(), symbol);
    }

    public Symbol getSymbol(@NonNull String identificator) {
        if (symbolExists(identificator)) {
            if (symbols.containsKey(identificator)) {
                return symbols.get(identificator);
            } else if (hasSuperScope()) {
                return superScope.getSymbol(identificator);
            }
        }
        return null;
    }

    public boolean symbolExists(@NonNull String identificator) {
        if (symbols.containsKey(identificator)) {
            return true;
        } else if (hasSuperScope()) {
            return superScope.symbolExists(identificator);
        }
        return false;
    }

    public boolean variableExists(@NonNull String identificator) {
        if (symbols.containsKey(identificator)) {
            return symbols.get(identificator).isVariable();
        } else if (hasSuperScope()) {
            return superScope.variableExists(identificator);
        }
        return false;
    }

    public boolean isLocal(@NonNull String identificator) {
        return symbols.containsKey(identificator);
    }

    public boolean procedureExists(@NonNull String identificator) {
        if (symbols.containsKey(identificator)) {
            return symbols.get(identificator).isProcedure();
        } else if (hasSuperScope()) {
            return superScope.procedureExists(identificator);
        }
        return false;
    }

    public SymbolScope subScope() {
        SymbolScope subScope = new SymbolScope(this);
        subScopes.add(subScope);
        return subScope;
    }

    public boolean hasSuperScope() {
        return superScope != null;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        final String scopeName = this.getClass().getSimpleName();
        final String scopeDescription = "%s[shape=oval, label=\"%s\"]\n";
        final String symbolDescription = "%s[shape=record, label=\"{%s|variable=%s|type=%s}\"]\n";
        final String connection = "%s->%s\n";
        final String uniqueAddress = Integer.toString(System.identityHashCode(this));
        sb.append(String.format(scopeDescription, uniqueAddress, scopeName));
        // SuperScope
        if (hasSuperScope()) {
            sb.append(String.format(connection, uniqueAddress, Integer.toString(System.identityHashCode(superScope))));
        }

        for (Symbol v : symbols.values()) {
            sb.append(String.format(symbolDescription, Integer.toString(System.identityHashCode(v)),
                    v.getIdentificator(), v.isVariable(), v.getType()));
            sb.append(String.format(connection, uniqueAddress, Integer.toString(System.identityHashCode(v))));
        }


        for (SymbolScope vs : subScopes) {
            sb.append(vs.toString());
            sb.append(String.format(connection, uniqueAddress, Integer.toString(System.identityHashCode(vs))));
        }

        return sb.toString();
    }
}
