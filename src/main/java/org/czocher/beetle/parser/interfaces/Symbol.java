package org.czocher.beetle.parser.interfaces;

import lombok.Data;

@Data
public class Symbol {
    private final String identificator;
    private SemanticType type;
    private boolean isVariable = true;
    private int index;

    public Symbol(String identificator, SemanticType type, boolean isVariable) {
        this.identificator = identificator;
        this.type = type;
        this.isVariable = isVariable;
    }

    public boolean isArray() {
        return type.isArrayType();
    }

    public boolean isVariable() {
        return isVariable;
    }

    public boolean isProcedure() {
        return !isVariable;
    }

    @Override
    public String toString() {
        return this.identificator;
    }

}
