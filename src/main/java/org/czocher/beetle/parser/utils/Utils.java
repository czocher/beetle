package org.czocher.beetle.parser.utils;

import com.sun.org.apache.bcel.internal.generic.LocalVariableGen;
import org.czocher.beetle.lexer.interfaces.LexemType;

import java.util.HashSet;
import java.util.Set;

public class Utils {
    public static Set<LexemType> sum(Set<LexemType> first, Set<LexemType> second) {
        Set<LexemType> ret = new HashSet<>();
        ret.addAll(first);
        ret.addAll(second);
        return ret;
    }

    public static LocalVariableGen getLocalVariableByName(LocalVariableGen[] lv, String name) {
        for (LocalVariableGen lvg : lv) {
            if (lvg.getName().equals(name)) {
                return lvg;
            }
        }
        return null;
    }
}
