package org.czocher.beetle.parser.utils;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

@RequiredArgsConstructor
public class DotRenderer {

    @NonNull
    private final String dot;
    @NonNull
    @Getter
    @Setter
    private String graphType = "digraph";
    @NonNull
    @Getter
    @Setter
    private String graphName = "Tree";

    public void render() {
        ProcessBuilder processbuilder = new ProcessBuilder("dot", "-Tpng");

        try {
            Process process = processbuilder.start();

            final InputStream processOutput = process.getInputStream();
            final OutputStream processInput = process.getOutputStream();

            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(processInput));
            writer.write(String.format(graphType + " " + graphName + " {\n%s\n}", dot));
            writer.close();
            BufferedImage bufferedImage = ImageIO.read(processOutput);
            processOutput.close();

            ImagePanel imagePanel = new ImagePanel(bufferedImage);

            JFrame frame = new JFrame();
            frame.add(new JScrollPane(imagePanel));
            frame.setSize(imagePanel.getPreferredSize());
            frame.setVisible(true);
            frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            frame.setTitle(getGraphName());
        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    class ImagePanel extends JPanel {

        private final BufferedImage bufferedImage;

        public ImagePanel(BufferedImage bufferedImage) {
            this.bufferedImage = bufferedImage;
        }

        @Override
        public Dimension getPreferredSize() {
            if(bufferedImage == null) {
                return super.getPreferredSize();
            } else {
                return new Dimension(bufferedImage.getWidth()+20, bufferedImage.getHeight()+20);
            }
        }

        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            if (bufferedImage != null) {
                this.setBackground(Color.white);

                int x = (getWidth() - bufferedImage.getWidth()) / 2;
                int y = (getHeight() - bufferedImage.getHeight()) / 2;

                g.drawImage(bufferedImage, x, y, this);

            }
        }
    }


}
