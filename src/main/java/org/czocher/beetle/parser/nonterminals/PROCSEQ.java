package org.czocher.beetle.parser.nonterminals;

import lombok.NonNull;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.Lexer;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.parser.interfaces.SemanticType;
import org.czocher.beetle.parser.interfaces.SymbolScope;
import org.czocher.beetle.parser.interfaces.TreeNode;

import java.util.Arrays;
import java.util.Set;

import static org.czocher.beetle.parser.utils.Utils.sum;

public class PROCSEQ extends TreeNode {

    public PROCSEQ(@NonNull Lexer lexer, @NonNull SymbolScope symbolScope,
                   @NonNull ErrorContainer errorContainer, @NonNull Set<LexemType> superFirstSet) {
        super(lexer, symbolScope, errorContainer, superFirstSet);
        this.firstSet.addAll(Arrays.asList(LexemType.SUB));
        this.followSet.addAll(Arrays.asList(LexemType.MAIN));
        this.setType(SemanticType.VOID);
    }

    @Override
    public TreeNode postParse() {
        TreeNode PROCDECL = new PROCDECL(lexer, symbolScope, errorContainer,
                sum(firstSet, superFirstSet)).parse();
        if (PROCDECL != null) {
            addChild(PROCDECL);
        }

        TreeNode PROCSEQ = new PROCSEQ(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (PROCSEQ != null) {
            addChildren(PROCSEQ.getChildren());
        }

        return this;
    }
}
