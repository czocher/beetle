package org.czocher.beetle.parser.nonterminals;

import lombok.NonNull;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.Lexer;
import org.czocher.beetle.lexer.interfaces.Lexem;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.parser.interfaces.SemanticType;
import org.czocher.beetle.parser.interfaces.SymbolScope;
import org.czocher.beetle.parser.interfaces.TreeNode;

import java.util.Arrays;
import java.util.Set;

import static org.czocher.beetle.parser.utils.Utils.sum;

public class LOOP extends TreeNode {

    public LOOP(@NonNull Lexer lexer, @NonNull SymbolScope symbolScope,
                @NonNull ErrorContainer errorContainer, @NonNull Set<LexemType> superFirstSet) {
        super(lexer, symbolScope, errorContainer, superFirstSet);
        this.firstSet.addAll(Arrays.asList(LexemType.DO));
        this.followSet.addAll(Arrays.asList(LexemType.SEMICOLON));
        this.setType(SemanticType.VOID);
    }

    @Override
    public TreeNode postParse() {
        acceptOrSkip(LexemType.DO);
        acceptOrSkip(LexemType.OPENING_PARENTHESIS);

        TreeNode EXPR = new EXPR(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (EXPR != null) {
            addChild(EXPR);
        } else {
            Lexem l = lexer.peek();
            errorContainer.addError("Expected EXPR.", l.getStartLine(),
                    l.getStartCol(), l.getEndLine(), l.getEndCol());
            return null;
        }

        acceptOrSkip(LexemType.CLOSING_PARENTHESIS);
        acceptOrSkip(LexemType.OPENING_BRACE);

        TreeNode INSTRSEQ = new INSTRSEQ(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (INSTRSEQ != null) {
            addChild(INSTRSEQ);
        }

        acceptOrSkip(LexemType.CLOSING_BRACE);

        return this;
    }
}

