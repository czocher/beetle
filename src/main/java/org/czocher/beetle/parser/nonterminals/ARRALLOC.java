package org.czocher.beetle.parser.nonterminals;

import com.sun.org.apache.bcel.internal.generic.*;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.Lexer;
import org.czocher.beetle.lexer.interfaces.Lexem;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.parser.interfaces.SemanticType;
import org.czocher.beetle.parser.interfaces.Symbol;
import org.czocher.beetle.parser.interfaces.SymbolScope;
import org.czocher.beetle.parser.interfaces.TreeNode;

import java.util.Arrays;
import java.util.Set;

public class ARRALLOC extends TreeNode {

    @Getter
    @Setter
    private Symbol symbol;

    @Getter
    @Setter
    private int size;

    public ARRALLOC(@NonNull Lexer lexer, @NonNull SymbolScope symbolScope,
                    @NonNull ErrorContainer errorContainer, @NonNull Set<LexemType> superFirstSet) {
        super(lexer, symbolScope, errorContainer, superFirstSet);
        this.firstSet.addAll(Arrays.asList(LexemType.ALLOC));
        this.followSet.addAll(Arrays.asList(LexemType.SEMICOLON));
        this.setType(SemanticType.VOID);
    }

    @Override
    public TreeNode postParse() {
        acceptOrSkip(LexemType.ALLOC);
        acceptOrSkip(LexemType.OPENING_PARENTHESIS);

        Lexem ident = lexer.peek();
        if (ident.getType() == LexemType.IDENT) {
            String varName = lexer.<String>nextLexem().getValue();
            log.debug("Accepted {}", ident);

            if (symbolScope.variableExists(varName)) {
                symbol = symbolScope.getSymbol(varName);
            } else {
                errorContainer.addError("Unknown variable " + varName, ident);
            }
        } else {
            errorContainer.addError("Expected IDENT, found " + ident.getType(), ident);
            return null;
        }

        acceptOrSkip(LexemType.COMMA);

        Lexem arSize = lexer.peek();
        if (arSize.getType() == LexemType.LINT) {
            size = lexer.<Integer>nextLexem().getValue();
        } else {
            errorContainer.addError("Expected LINT, found " + arSize.getType(), arSize);
            return null;
        }

        acceptOrSkip(LexemType.CLOSING_PARENTHESIS);

        return this;
    }

    @Override
    public void generateCode(ClassGen cg, MethodGen mg) {
        InstructionList il = mg.getInstructionList();
        super.generateCode(cg, mg);
        il.append(new PUSH(cg.getConstantPool(), size));
        switch (symbol.getType()) {
            case ARRAY_INTEGER:
                il.append(new NEWARRAY(Type.INT));
                break;
            case ARRAY_FLOAT:
                il.append(new NEWARRAY(Type.FLOAT));
                break;
            case ARRAY_BOOLEAN:
                il.append(new NEWARRAY(Type.BOOLEAN));
                break;
            case ARRAY_STRING:
                il.append(new InstructionFactory(cg).createNewArray(Type.STRING, (short) 1));
                break;
        }
        il.append(new InstructionFactory(cg).createPutStatic("Output", symbol.getIdentificator(), symbol.getType().javaType()));
    }
}
