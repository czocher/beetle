package org.czocher.beetle.parser.nonterminals;

import lombok.NonNull;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.Lexer;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.parser.interfaces.SemanticType;
import org.czocher.beetle.parser.interfaces.SymbolScope;
import org.czocher.beetle.parser.interfaces.TreeNode;

import java.util.Arrays;
import java.util.Set;

import static org.czocher.beetle.parser.utils.Utils.sum;

public class EXPR2 extends TreeNode {

    public EXPR2(@NonNull Lexer lexer, @NonNull SymbolScope symbolScope,
                 @NonNull ErrorContainer errorContainer, @NonNull Set<LexemType> superFirstSet) {
        super(lexer, symbolScope, errorContainer, superFirstSet);
        this.firstSet.addAll(Arrays.asList(LexemType.RELOP));
        this.followSet.addAll(Arrays.asList(LexemType.COMMA, LexemType.CLOSING_PARENTHESIS));
        this.setType(SemanticType.VOID);
    }

    @Override
    public TreeNode postParse() {
        TreeNode RELOP = new RELOP(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        addChild(RELOP);

        TreeNode SIMPEXPR = new SIMPEXPR(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (SIMPEXPR != null) {
            RELOP.addChild(SIMPEXPR.getChild(0));
        }

        TreeNode EXPR2 = new EXPR2(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (EXPR2 != null) {
            addChildren(EXPR2.getChildren());
        }

        return this;
    }
}
