package org.czocher.beetle.parser.nonterminals;

import com.sun.org.apache.bcel.internal.generic.*;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.Lexer;
import org.czocher.beetle.lexer.interfaces.Lexem;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.lexer.lexems.literals.IDENTLexem;
import org.czocher.beetle.parser.interfaces.SemanticType;
import org.czocher.beetle.parser.interfaces.Symbol;
import org.czocher.beetle.parser.interfaces.SymbolScope;
import org.czocher.beetle.parser.interfaces.TreeNode;

import java.util.Arrays;
import java.util.Set;

import static org.czocher.beetle.parser.utils.Utils.getLocalVariableByName;
import static org.czocher.beetle.parser.utils.Utils.sum;

public class ASSIGN extends TreeNode {

    @Getter
    @Setter
    private Symbol symbol;

    @Getter
    @Setter
    private Symbol symbolIndex;

    @Getter
    @Setter
    private Integer integerIndex;

    public ASSIGN(@NonNull Lexer lexer, @NonNull SymbolScope symbolScope,
                  @NonNull ErrorContainer errorContainer, @NonNull Set<LexemType> superFirstSet) {
        super(lexer, symbolScope, errorContainer, superFirstSet);
        this.firstSet.addAll(Arrays.asList(LexemType.IDENT));
        this.followSet.addAll(Arrays.asList(LexemType.SEMICOLON));
        this.setType(SemanticType.VOID);
    }

    @Override
    public TreeNode postParse() {
        IDENTLexem ident = (IDENTLexem) lexer.<String>nextLexem();
        String varName = ident.getValue();
        if (symbolScope.variableExists(varName) || symbolScope.procedureExists(varName)) {
            symbol = symbolScope.getSymbol(varName);
        } else {
            errorContainer.addError("Unknown variable " + varName, ident);
            return null;
        }
        log.debug("Accepted {}", lexer.currentLexem());

        if (lexer.peek().getType() == LexemType.OPENING_PARENTHESIS) {
            acceptOrSkip(LexemType.OPENING_PARENTHESIS);
            switch (lexer.peek().getType()) {
                case LINT:
                    integerIndex = lexer.<Integer>nextLexem().getValue();
                    break;
                case IDENT:
                    parseArrayIndexSymbol();
                    break;
                default:
                    errorContainer.addError("Expected IDENT or LINT, found: " + lexer.peek().getType() + ".",
                            lexer.peek());
                    return null;
            }

            acceptOrSkip(LexemType.CLOSING_PARENTHESIS);
        }

        if (integerIndex != null && !symbol.isArray()) {
            errorContainer.addError("Variable is not an array " + varName, ident);
        } else if (integerIndex == null && symbol.isArray()) {
            errorContainer.addError("Variable index not specified " + varName, ident);
        }

        acceptOrSkip(LexemType.EQ);

        TreeNode EXPR = new EXPR(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (EXPR != null) {
            addChildren(EXPR.getChildren());
        } else {
            Lexem l = lexer.peek();
            errorContainer.addError("Expected EXPR.", l.getStartLine(),
                    l.getStartCol(), l.getEndLine(), l.getEndCol());
            return null;
        }

        return this;
    }

    private void parseArrayIndexSymbol() {
        String indexVarName = lexer.<String>nextLexem().getValue();
        if (symbolScope.variableExists(indexVarName)) {
            symbolIndex = symbolScope.getSymbol(indexVarName);
        } else {
            errorContainer.addError("Unknown variable " + indexVarName, lexer.currentLexem());
        }
    }

    @Override
    public void generateCode(ClassGen cg, MethodGen mg) {
        InstructionList il = mg.getInstructionList();

        if (!symbolScope.isLocal(symbol.getIdentificator())) {
            if (symbol.isArray()) {
                il.append(new InstructionFactory(cg).createGetStatic("Output", symbol.getIdentificator(), symbol.getType().javaType()));
                if (integerIndex != null) {
                    il.append(new PUSH(cg.getConstantPool(), integerIndex));
                } else if (symbolIndex != null) {
                    // TODO: Local symbol or global symbol
                    il.append(new InstructionFactory(cg).createGetStatic("Output", symbolIndex.getIdentificator(), symbolIndex.getType().javaType()));
                }
                super.generateCode(cg, mg);
                il.append(InstructionConstants.IASTORE);
            } else if (symbol.isVariable()) {
                super.generateCode(cg, mg);
                il.append(new InstructionFactory(cg).createPutStatic("Output", symbol.getIdentificator(), symbol.getType().javaType()));
            } else if (symbol.isProcedure()) {
                super.generateCode(cg, mg);
                il.append(new InstructionFactory(cg).createStore(symbol.getType().javaType(), getLocalVariableByName(mg.getLocalVariables(), symbol.getIdentificator()).getIndex()));
            }
        } else {
            if (symbol.isArray()) {
                il.append(new InstructionFactory(cg).createLoad(symbol.getType().javaType(), getLocalVariableByName(mg.getLocalVariables(), symbol.getIdentificator()).getIndex()));
                if (integerIndex != null) {
                    il.append(new PUSH(cg.getConstantPool(), integerIndex));
                } else if (symbolIndex != null) {
                    // TODO: Local symbol or global symbol
                    il.append(new InstructionFactory(cg).createLoad(symbolIndex.getType().javaType(), getLocalVariableByName(mg.getLocalVariables(), symbolIndex.getIdentificator()).getIndex()));
                }
                super.generateCode(cg, mg);
                il.append(InstructionConstants.IASTORE);
            } else if (symbol.isVariable()) {
                super.generateCode(cg, mg);
                il.append(new InstructionFactory(cg).createStore(symbol.getType().javaType(), getLocalVariableByName(mg.getLocalVariables(), symbol.getIdentificator()).getIndex()));
            }
        }
    }
}
