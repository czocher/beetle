package org.czocher.beetle.parser.nonterminals;

import lombok.NonNull;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.Lexer;
import org.czocher.beetle.lexer.interfaces.Lexem;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.parser.interfaces.SemanticType;
import org.czocher.beetle.parser.interfaces.SymbolScope;
import org.czocher.beetle.parser.interfaces.TreeNode;

import java.util.Arrays;
import java.util.Set;

import static org.czocher.beetle.parser.utils.Utils.sum;

public class ATYPE extends TreeNode {

    public ATYPE(@NonNull Lexer lexer, @NonNull SymbolScope symbolScope,
                 @NonNull ErrorContainer errorContainer, @NonNull Set<LexemType> superFirstSet) {
        super(lexer, symbolScope, errorContainer, superFirstSet);
        this.firstSet.addAll(Arrays.asList(LexemType.ARRAYOF));
        this.followSet.addAll(Arrays.asList(LexemType.SEMICOLON));
        this.setType(SemanticType.VOID);
    }

    @Override
    public TreeNode postParse() {
        acceptOrSkip(LexemType.ARRAYOF);

        TreeNode TYPEIDENT = new TYPEIDENT(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (TYPEIDENT != null && !((TYPEIDENT) TYPEIDENT).getValue().isArrayType()) {
            addChild(TYPEIDENT);
        } else if (TYPEIDENT != null && ((TYPEIDENT) TYPEIDENT).getValue().isArrayType()) {
            Lexem<String> lexem = lexer.<String>peek();
            errorContainer.addError("Array type cannot have array subtype: " + lexem, lexem);
            return null;
        } else {
            Lexem<String> lexem = lexer.<String>peek();
            errorContainer.addError("Unknown variable type: " + lexem, lexem);
            return null;
        }

        return this;
    }
}
