package org.czocher.beetle.parser.nonterminals;

import lombok.NonNull;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.Lexer;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.parser.interfaces.SemanticType;
import org.czocher.beetle.parser.interfaces.SymbolScope;
import org.czocher.beetle.parser.interfaces.TreeNode;

import java.util.Arrays;
import java.util.Set;

import static org.czocher.beetle.parser.utils.Utils.sum;

public class SIMPEXPR extends TreeNode {

    public SIMPEXPR(@NonNull Lexer lexer, @NonNull SymbolScope symbolScope,
                    @NonNull ErrorContainer errorContainer, @NonNull Set<LexemType> superFirstSet) {
        super(lexer, symbolScope, errorContainer, superFirstSet);
        this.firstSet.addAll(Arrays.asList(LexemType.IDENT, LexemType.LSTRING, LexemType.LINT, LexemType.LBOOL,
                LexemType.LREAL, LexemType.CALL));
        this.followSet.addAll(Arrays.asList(LexemType.RELOP));
        this.setType(SemanticType.UNKNOWN);
    }

    @Override
    public TreeNode postParse() {

        TreeNode TERM = new TERM(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (TERM != null) {
            addChildren(TERM.getChildren());
        }

        TreeNode SIMPEXPR2 = new SIMPEXPR2(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (SIMPEXPR2 != null) {
            addChildren(SIMPEXPR2.getChildren());
        }

        while (children.size() > 1) {
            getChild(1).addChild(0, getChild(0));
            removeChild(0);
        }

        return this;
    }
}
