package org.czocher.beetle.parser.nonterminals;

import lombok.NonNull;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.Lexer;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.parser.interfaces.SemanticType;
import org.czocher.beetle.parser.interfaces.SymbolScope;
import org.czocher.beetle.parser.interfaces.TreeNode;

import java.util.Arrays;
import java.util.Set;

import static org.czocher.beetle.parser.utils.Utils.sum;

public class INSTR extends TreeNode {

    public INSTR(@NonNull Lexer lexer, @NonNull SymbolScope symbolScope,
                 @NonNull ErrorContainer errorContainer, @NonNull Set<LexemType> superFirstSet) {
        super(lexer, symbolScope, errorContainer, superFirstSet);
        this.firstSet.addAll(Arrays.asList(LexemType.IDENT, LexemType.IF, LexemType.DO,
                LexemType.READ, LexemType.PRINT, LexemType.BLOCK, LexemType.CALL, LexemType.ALLOC));
        this.followSet.addAll(Arrays.asList(LexemType.SEMICOLON));
        this.setType(SemanticType.VOID);
    }

    @Override
    public TreeNode postParse() {
        TreeNode ASSIGN = new ASSIGN(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (ASSIGN != null) {
            addChild(ASSIGN);
        }

        TreeNode COND = new COND(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (COND != null) {
            addChild(COND);
        }

        TreeNode LOOP = new LOOP(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (LOOP != null) {
            addChild(LOOP);
        }

        TreeNode IN = new IN(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (IN != null) {
            addChild(IN);
        }

        TreeNode OUT = new OUT(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (OUT != null) {
            addChild(OUT);
        }

        TreeNode BLOCK = new BLOCK(lexer, symbolScope, errorContainer,
                sum(firstSet, superFirstSet)).parse();
        if (BLOCK != null) {
            addChild(BLOCK);
        }

        TreeNode SCALL = new SCALL(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (SCALL != null) {
            addChild(SCALL);
        }

        TreeNode ARRALLOC = new ARRALLOC(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (ARRALLOC != null) {
            addChild(ARRALLOC);
        }

        if (!children.isEmpty()) {
            return this;
        } else {
            return null;
        }
    }

}
