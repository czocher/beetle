package org.czocher.beetle.parser.nonterminals;

import lombok.NonNull;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.Lexer;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.parser.interfaces.SemanticType;
import org.czocher.beetle.parser.interfaces.SymbolScope;
import org.czocher.beetle.parser.interfaces.TreeNode;

import java.util.Arrays;
import java.util.Set;

import static org.czocher.beetle.parser.utils.Utils.sum;

public class APARAMS2 extends TreeNode {

    public APARAMS2(@NonNull Lexer lexer, @NonNull SymbolScope symbolScope,
                    @NonNull ErrorContainer errorContainer, @NonNull Set<LexemType> superFirstSet) {
        super(lexer, symbolScope, errorContainer, superFirstSet);
        this.firstSet.addAll(Arrays.asList(LexemType.COMMA));
        this.followSet.addAll(Arrays.asList(LexemType.CLOSING_PARENTHESIS));
        this.setType(SemanticType.VOID);
    }

    @Override
    public TreeNode postParse() {
        acceptOrSkip(LexemType.COMMA);

        TreeNode APARAMS = new APARAMS(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (APARAMS != null) {
            addChild(APARAMS);
        }

        return this;
    }
}
