package org.czocher.beetle.parser.nonterminals;

import lombok.NonNull;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.Lexer;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.parser.interfaces.SemanticType;
import org.czocher.beetle.parser.interfaces.SymbolScope;
import org.czocher.beetle.parser.interfaces.TreeNode;

import java.util.Arrays;
import java.util.Set;

import static org.czocher.beetle.parser.utils.Utils.sum;

public class FPARAMS extends TreeNode {

    public FPARAMS(@NonNull Lexer lexer, @NonNull SymbolScope symbolScope,
                   @NonNull ErrorContainer errorContainer, @NonNull Set<LexemType> superFirstSet) {
        super(lexer, symbolScope, errorContainer, superFirstSet);
        this.firstSet.addAll(Arrays.asList(LexemType.IDENT));
        this.followSet.addAll(Arrays.asList(LexemType.CLOSING_PARENTHESIS));
        this.setType(SemanticType.VOID);
    }

    @Override
    public TreeNode postParse() {

        TreeNode FPARAM = new FPARAM(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (FPARAM != null) {
            addChild(FPARAM);
        }

        TreeNode FPARAMS2 = new FPARAMS2(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (FPARAMS2 != null) {
            addChildren(FPARAMS2.getChild(0).getChildren());
        }

        return this;
    }
}
