package org.czocher.beetle.parser.nonterminals;

import lombok.NonNull;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.Lexer;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.parser.interfaces.SemanticType;
import org.czocher.beetle.parser.interfaces.SymbolScope;
import org.czocher.beetle.parser.interfaces.TreeNode;

import java.util.Arrays;
import java.util.Set;

import static org.czocher.beetle.parser.utils.Utils.sum;

public class TERM extends TreeNode {

    public TERM(@NonNull Lexer lexer, @NonNull SymbolScope symbolScope,
                @NonNull ErrorContainer errorContainer, @NonNull Set<LexemType> superFirstSet) {
        super(lexer, symbolScope, errorContainer, superFirstSet);
        this.firstSet.addAll(Arrays.asList(LexemType.IDENT, LexemType.LSTRING, LexemType.LINT, LexemType.LBOOL,
                LexemType.LREAL, LexemType.CALL));
        this.followSet.addAll(Arrays.asList(LexemType.ADDOP));
        this.setType(SemanticType.UNKNOWN);
    }

    @Override
    public TreeNode postParse() {
        TreeNode FACTOR = new FACTOR(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        addChild(FACTOR);


        TreeNode TERM2 = new TERM2(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (TERM2 != null) {
            addChildren(TERM2.getChildren());
        }

        while (children.size() > 1) {
            getChild(1).addChild(0, getChild(0));
            removeChild(0);
        }

        return this;
    }
}
