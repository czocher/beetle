package org.czocher.beetle.parser.nonterminals;

import lombok.NonNull;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.Lexer;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.parser.interfaces.SemanticType;
import org.czocher.beetle.parser.interfaces.SymbolScope;
import org.czocher.beetle.parser.interfaces.TreeNode;

import java.util.Arrays;
import java.util.Set;

import static org.czocher.beetle.parser.utils.Utils.sum;

public class VARSEQ extends TreeNode {

    public VARSEQ(@NonNull Lexer lexer, @NonNull SymbolScope symbolScope,
                  @NonNull ErrorContainer errorContainer, @NonNull Set<LexemType> superFirstSet) {
        super(lexer, symbolScope, errorContainer, superFirstSet);
        this.firstSet.addAll(Arrays.asList(LexemType.VAR));
        this.followSet.addAll(Arrays.asList(LexemType.MAIN, LexemType.SUB));
        this.setType(SemanticType.VOID);
    }

    @Override
    public TreeNode postParse() {
        TreeNode VARDECL = new VARDECL(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (VARDECL != null) {
            addChild(VARDECL);
        }

        acceptOrSkip(LexemType.SEMICOLON);

        TreeNode VARSEQ = new VARSEQ(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (VARSEQ != null) {
            addChildren(VARSEQ.getChildren());
        }

        return this;
    }
}
