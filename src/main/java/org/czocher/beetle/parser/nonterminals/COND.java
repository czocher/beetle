package org.czocher.beetle.parser.nonterminals;

import com.sun.org.apache.bcel.internal.Constants;
import com.sun.org.apache.bcel.internal.generic.*;
import lombok.NonNull;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.Lexer;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.parser.interfaces.SemanticType;
import org.czocher.beetle.parser.interfaces.SymbolScope;
import org.czocher.beetle.parser.interfaces.TreeNode;

import java.util.Arrays;
import java.util.Set;

import static org.czocher.beetle.parser.utils.Utils.sum;

public class COND extends TreeNode {

    public COND(@NonNull Lexer lexer, @NonNull SymbolScope symbolScope,
                @NonNull ErrorContainer errorContainer, @NonNull Set<LexemType> superFirstSet) {
        super(lexer, symbolScope, errorContainer, superFirstSet);
        this.firstSet.addAll(Arrays.asList(LexemType.IF));
        this.followSet.addAll(Arrays.asList(LexemType.SEMICOLON));
        this.setType(SemanticType.VOID);
    }

    @Override
    public TreeNode postParse() {
        acceptOrSkip(LexemType.IF);
        acceptOrSkip(LexemType.OPENING_PARENTHESIS);

        TreeNode EXPR = new EXPR(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (EXPR != null) {
            addChildren(EXPR.getChildren());
        } else {
            errorContainer.addError("Expected EXPR.", lexer.peek());
            return null;
        }

        acceptOrSkip(LexemType.CLOSING_PARENTHESIS);
        acceptOrSkip(LexemType.OPENING_BRACE);

        TreeNode INSTRSEQ = new INSTRSEQ(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (INSTRSEQ != null) {
            addChild(INSTRSEQ);
        }

        acceptOrSkip(LexemType.CLOSING_BRACE);

        return this;
    }

    @Override
    public void generateCode(ClassGen cg, MethodGen mg) {
        getChild(0).generateCode(cg, mg);
        BranchInstruction bi = new InstructionFactory(cg).createBranchInstruction(Constants.IFEQ, mg.getInstructionList().getStart());
        mg.getInstructionList().append(bi);

        getChild(1).generateCode(cg, mg);
        mg.getInstructionList().append(InstructionConstants.NOP);
        bi.setTarget(mg.getInstructionList().getEnd());

    }
}

