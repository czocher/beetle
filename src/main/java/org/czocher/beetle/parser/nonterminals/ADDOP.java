package org.czocher.beetle.parser.nonterminals;

import com.sun.org.apache.bcel.internal.generic.ClassGen;
import com.sun.org.apache.bcel.internal.generic.InstructionConstants;
import com.sun.org.apache.bcel.internal.generic.InstructionList;
import com.sun.org.apache.bcel.internal.generic.MethodGen;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.Lexer;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.parser.interfaces.SemanticType;
import org.czocher.beetle.parser.interfaces.SymbolScope;
import org.czocher.beetle.parser.interfaces.TreeNode;

import java.util.Arrays;
import java.util.Set;

public class ADDOP extends TreeNode {

    @Getter
    @Setter
    private String operation;

    public ADDOP(@NonNull Lexer lexer, @NonNull SymbolScope symbolScope,
                 @NonNull ErrorContainer errorContainer, @NonNull Set<LexemType> superFirstSet) {
        super(lexer, symbolScope, errorContainer, superFirstSet);
        this.firstSet.addAll(Arrays.asList(LexemType.ADDOP));
        this.followSet.addAll(Arrays.asList(LexemType.IDENT, LexemType.LSTRING, LexemType.LINT, LexemType.LBOOL,
                LexemType.LREAL, LexemType.CALL));
        this.setType(SemanticType.UNKNOWN);
    }

    @Override
    public TreeNode postParse() {
        operation = lexer.<String>nextLexem().getValue();
        log.debug("Accepted {}", lexer.currentLexem());
        return this;
    }

    @Override
    public void generateCode(ClassGen cg, MethodGen mg) {
        InstructionList il = mg.getInstructionList();
        super.generateCode(cg, mg);
        // TODO: String?
        switch (operation) {
            case "+":
                switch (getType()) {
                    case INTEGER:
                        il.append(InstructionConstants.IADD);
                        break;
                    case FLOAT:
                        il.append(InstructionConstants.FADD);
                        break;
                    case BOOLEAN:
                        il.append(InstructionConstants.IADD);
                        break;
                }
                break;
            case "-":
                switch (getType()) {
                    case INTEGER:
                        il.append(InstructionConstants.ISUB);
                        break;
                    case FLOAT:
                        il.append(InstructionConstants.FSUB);
                        break;
                    case BOOLEAN:
                        il.append(InstructionConstants.ISUB);
                        break;
                }
                break;
            case "or":
                switch (getType()) {
                    case BOOLEAN:
                        il.append(InstructionConstants.IOR);
                        break;
                    case INTEGER:
                        il.append(InstructionConstants.IOR);
                        break;
                    case FLOAT:
                        errorContainer.addError("Cannot perform or on floats!", startLine, startCol, endLine, endCol);
                        break;
                }
                break;
        }

    }
}
