package org.czocher.beetle.parser.nonterminals;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.Lexer;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.lexer.lexems.literals.IDENTLexem;
import org.czocher.beetle.parser.interfaces.SemanticType;
import org.czocher.beetle.parser.interfaces.Symbol;
import org.czocher.beetle.parser.interfaces.SymbolScope;
import org.czocher.beetle.parser.interfaces.TreeNode;

import java.util.Arrays;
import java.util.Set;

import static org.czocher.beetle.parser.utils.Utils.sum;

public class FPARAM extends TreeNode {

    @Getter
    @Setter
    private Symbol symbol;

    public FPARAM(@NonNull Lexer lexer, @NonNull SymbolScope symbolScope,
                  @NonNull ErrorContainer errorContainer, @NonNull Set<LexemType> superFirstSet) {
        super(lexer, symbolScope, errorContainer, superFirstSet);
        this.firstSet.addAll(Arrays.asList(LexemType.IDENT));
        this.followSet.addAll(Arrays.asList(LexemType.COMMA));
        this.setType(SemanticType.VOID);
    }

    @Override
    public TreeNode postParse() {
        IDENTLexem ident = (IDENTLexem) lexer.<String>nextLexem();
        log.debug("Accepted {}", ident);

        acceptOrSkip(LexemType.COLON);

        TYPEIDENT TYPEIDENT = (TYPEIDENT) new TYPEIDENT(lexer, symbolScope, errorContainer,
                sum(firstSet, superFirstSet)).parse();
        if (TYPEIDENT != null) {
            type = TYPEIDENT.getValue();
        }

        symbol = new Symbol(ident.getValue(), type, true);
        symbolScope.addSymbol(symbol);

        return this;
    }
}
