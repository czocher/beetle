package org.czocher.beetle.parser.nonterminals;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.Lexer;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.parser.interfaces.SemanticType;
import org.czocher.beetle.parser.interfaces.SymbolScope;
import org.czocher.beetle.parser.interfaces.TreeNode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.czocher.beetle.parser.utils.Utils.sum;

public class IDLIST extends TreeNode {

    @Getter
    @Setter
    private List<String> value;

    public IDLIST(@NonNull Lexer lexer, @NonNull SymbolScope symbolScope,
                  @NonNull ErrorContainer errorContainer, @NonNull Set<LexemType> superFirstSet) {
        super(lexer, symbolScope, errorContainer, superFirstSet);
        this.value = new ArrayList<>();
        this.firstSet.addAll(Arrays.asList(LexemType.IDENT));
        this.followSet.addAll(Arrays.asList(LexemType.COLON));
        this.setType(SemanticType.VOID);
    }

    @Override
    public TreeNode postParse() {
        value.add(lexer.<String>nextLexem().getValue());
        log.debug("Accepted {}", lexer.currentLexem());

        TreeNode IDLIST2 = new IDLIST2(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (IDLIST2 != null) {
            value.addAll(((IDLIST) IDLIST2.getChild(0)).getValue());
        }

        return this;
    }
}
