package org.czocher.beetle.parser.nonterminals;

import com.sun.org.apache.bcel.internal.Constants;
import com.sun.org.apache.bcel.internal.generic.*;
import lombok.NonNull;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.Lexer;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.parser.interfaces.SemanticType;
import org.czocher.beetle.parser.interfaces.SymbolScope;
import org.czocher.beetle.parser.interfaces.TreeNode;

import java.util.Arrays;

public class PROGRAM extends TreeNode {

    public PROGRAM(@NonNull Lexer lexer, @NonNull SymbolScope symbolScope,
                   @NonNull ErrorContainer errorContainer) {
        super(lexer, symbolScope, errorContainer);
        firstSet.addAll(Arrays.asList(LexemType.MAIN, LexemType.VAR, LexemType.SUB));
        followSet.addAll(Arrays.asList(LexemType.EOF));
        this.setType(SemanticType.VOID);
    }

    @Override
    public TreeNode postParse() {
        TreeNode VARSEQ = new VARSEQ(lexer, symbolScope, errorContainer, firstSet).parse();
        if (VARSEQ != null) {
            addChild(VARSEQ);
        }

        TreeNode PROCSEQ = new PROCSEQ(lexer, symbolScope, errorContainer, firstSet).parse();
        if (PROCSEQ != null) {
            addChild(PROCSEQ);
        }

        acceptOrSkip(LexemType.MAIN);
        acceptOrSkip(LexemType.OPENING_BRACE);

        TreeNode INSTRSEQ = new INSTRSEQ(lexer, symbolScope.subScope(), errorContainer, firstSet).parse();
        if (INSTRSEQ != null) {
            addChild(INSTRSEQ);
        }

        acceptOrSkip(LexemType.CLOSING_BRACE);

        return this;
    }

    @Override
    public void generateCode(ClassGen cg, MethodGen mg) {
        InstructionList il = new InstructionList();
        MethodGen mg2 = new MethodGen(Constants.ACC_PUBLIC
                | Constants.ACC_STATIC, Type.VOID, new Type[]{new ArrayType(
                Type.STRING, 1)}, new String[]{"args"}, "main", "Output",
                il, cg.getConstantPool());

        if (getChild(0) instanceof VARSEQ) {
            getChild(0).generateCode(cg, null);
        } else {
            getChild(0).generateCode(cg, mg2);
        }

        for (int i = 1; i < getChildren().size(); i++) {
            getChild(i).generateCode(cg, mg2);
        }

        il.append(InstructionConstants.RETURN);
        mg2.setMaxStack();
        cg.addMethod(mg2.getMethod());
    }
}
