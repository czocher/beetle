package org.czocher.beetle.parser.nonterminals;

import com.sun.org.apache.bcel.internal.generic.*;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.Lexer;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.lexer.lexems.literals.IDENTLexem;
import org.czocher.beetle.parser.interfaces.SemanticType;
import org.czocher.beetle.parser.interfaces.Symbol;
import org.czocher.beetle.parser.interfaces.SymbolScope;
import org.czocher.beetle.parser.interfaces.TreeNode;

import java.util.Arrays;
import java.util.Set;

import static org.czocher.beetle.parser.utils.Utils.getLocalVariableByName;
import static org.czocher.beetle.parser.utils.Utils.sum;

public class FACTOR extends TreeNode {

    @Getter
    @Setter
    private Symbol symbol;
    @Getter
    @Setter
    private Object value;
    @Getter
    @Setter
    private Integer integerIndex;
    @Getter
    @Setter
    private Symbol symbolIndex;


    public FACTOR(@NonNull Lexer lexer, @NonNull SymbolScope symbolScope,
                  @NonNull ErrorContainer errorContainer, @NonNull Set<LexemType> superFirstSet) {
        super(lexer, symbolScope, errorContainer, superFirstSet);
        this.firstSet.addAll(Arrays.asList(LexemType.IDENT, LexemType.LSTRING, LexemType.LINT, LexemType.LBOOL,
                LexemType.LREAL, LexemType.CALL));
        this.followSet.addAll(Arrays.asList(LexemType.MULTOP));
        this.setType(SemanticType.VOID);
    }

    @Override
    public TreeNode postParse() {
        LexemType next = lexer.peek().getType();

        switch (next) {
            case IDENT:
                parseIdent();
                break;
            case LSTRING:
            case LINT:
            case LBOOL:
            case LREAL:
                setType(SemanticType.fromLiteralLexem(lexer.peek()));
                value = lexer.nextLexem().getValue();
                log.debug("Accepted {}", lexer.currentLexem());
                return this;
        }

        TreeNode SCALL = new SCALL(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (SCALL != null) {
            return SCALL;
        }

        return this;
    }

    private void parseIdent() {
        IDENTLexem ident = (IDENTLexem) lexer.<String>nextLexem();
        log.debug("Accepted {}", ident);

        String varName = ident.getValue();
        parseArrayIndex();

        if (symbolScope.variableExists(varName)) {
            symbol = symbolScope.getSymbol(varName);
            setType(symbol.getType());
            if (integerIndex != null && symbolIndex != null && !symbol.isArray()) {
                errorContainer.addError("Variable is not an array " + varName, ident);
            } else if (integerIndex == null && symbolIndex == null && symbol.isArray()) {
                errorContainer.addError("Variable index not specified " + varName, ident);
            }
        } else {
            errorContainer.addError("Unknown variable " + varName, ident);
        }
    }

    private void parseArrayIndex() {
        if (lexer.peek().getType() != LexemType.OPENING_PARENTHESIS) {
            return;
        }

        acceptOrSkip(LexemType.OPENING_PARENTHESIS);
        switch (lexer.peek().getType()) {
            case IDENT:
                IDENTLexem indexLexem = (IDENTLexem) lexer.<String>nextLexem();
                log.debug("Accepted {}", indexLexem);

                String indexVarName = indexLexem.getValue();
                if (symbolScope.variableExists(indexVarName)) {
                    symbolIndex = symbolScope.getSymbol(indexVarName);
                } else {
                    errorContainer.addError("Unknown variable " + indexVarName, indexLexem);
                }
                break;
            case LINT:
                integerIndex = lexer.<Integer>nextLexem().getValue();
                break;
            default:
                errorContainer.addError("Expected LINT or IDENT found: " + lexer.peek(), lexer.peek());
                return;
        }

        log.debug("Accepted {}", lexer.currentLexem());

        acceptOrSkip(LexemType.CLOSING_PARENTHESIS);
    }

    @Override
    public void generateCode(ClassGen cg, MethodGen mg) {
        InstructionList il = mg.getInstructionList();
        if (value != null) {
            switch (getType()) {
                case FLOAT:
                    il.append(new PUSH(cg.getConstantPool(), (Float) value));
                    break;
                case BOOLEAN:
                    il.append(new PUSH(cg.getConstantPool(), (Boolean) value));
                    break;
                case STRING:
                    il.append(new PUSH(cg.getConstantPool(), (String) value));
                    break;
                case INTEGER:
                    il.append(new PUSH(cg.getConstantPool(), (Integer) value));
                    break;
            }
        } else if (symbol != null && !symbolScope.isLocal(symbol.getIdentificator())) {
            if (symbol.isArray()) {
                il.append(new InstructionFactory(cg).createGetStatic("Output", symbol.getIdentificator(), symbol.getType().javaType()));
                // TODO: Local index
                if (integerIndex != null) {
                    il.append(new PUSH(cg.getConstantPool(), integerIndex));
                } else {
                    il.append(new InstructionFactory(cg).createGetStatic("Output", symbolIndex.getIdentificator(), symbolIndex.getType().javaType()));
                }
                switch (symbol.getType()) {
                    case ARRAY_INTEGER:
                    case ARRAY_BOOLEAN:
                        il.append(InstructionConstants.IALOAD);
                        break;
                    case ARRAY_FLOAT:
                        il.append(InstructionConstants.FALOAD);
                        break;
                    case ARRAY_STRING:
                        il.append(InstructionConstants.AALOAD);
                        break;
                }
            } else if (symbol.isProcedure()) {
                errorContainer.addError("Cannot operate on procedures", startLine, startCol, endLine, endCol);
            } else {
                il.append(new InstructionFactory(cg).createGetStatic("Output", symbol.getIdentificator(), symbol.getType().javaType()));
            }
        } else if (symbol != null && symbolScope.isLocal(symbol.getIdentificator())) {
            il.append(new InstructionFactory(cg).createLoad(symbol.getType().javaType(), getLocalVariableByName(mg.getLocalVariables(), symbol.getIdentificator()).getIndex()));
            // TODO: Arrays
            // TODO: Local index
        }
        super.generateCode(cg, mg);
    }
}
