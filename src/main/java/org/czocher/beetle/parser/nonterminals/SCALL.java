package org.czocher.beetle.parser.nonterminals;

import com.sun.org.apache.bcel.internal.Constants;
import com.sun.org.apache.bcel.internal.generic.*;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.Lexer;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.parser.interfaces.SemanticType;
import org.czocher.beetle.parser.interfaces.Symbol;
import org.czocher.beetle.parser.interfaces.SymbolScope;
import org.czocher.beetle.parser.interfaces.TreeNode;

import java.util.Arrays;
import java.util.Set;

import static org.czocher.beetle.parser.utils.Utils.sum;

public class SCALL extends TreeNode {

    @Getter
    @Setter
    private Symbol symbol;

    public SCALL(@NonNull Lexer lexer, @NonNull SymbolScope symbolScope,
                 @NonNull ErrorContainer errorContainer, @NonNull Set<LexemType> superFirstSet) {
        super(lexer, symbolScope, errorContainer, superFirstSet);
        this.firstSet.addAll(Arrays.asList(LexemType.CALL));
        this.followSet.addAll(Arrays.asList(LexemType.SEMICOLON));
        this.setType(SemanticType.VOID);
    }

    @Override
    public TreeNode postParse() {
        acceptOrSkip(LexemType.CALL);

        if (parseIdent()) {
            if (!parseArgs()) {
                return null;
            }
        } else {
            return null;
        }

        return this;
    }

    private boolean parseArgs() {
        if (lexer.peek().getType() == LexemType.OPENING_PARENTHESIS) {
            acceptOrSkip(LexemType.OPENING_PARENTHESIS);
            TreeNode APARAMS = new APARAMS(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
            if (APARAMS != null) {
                addChild(APARAMS);
            } else {
                errorContainer.addError("Expected APARAMS.", lexer.peek());
                return false;
            }
            acceptOrSkip(LexemType.CLOSING_PARENTHESIS);
        }
        return true;
    }

    private boolean parseIdent() {
        if (lexer.peek().getType() == LexemType.IDENT) {
            String procedureName = lexer.<String>nextLexem().getValue();
            if (symbolScope.procedureExists(procedureName)) {
                symbol = symbolScope.getSymbol(procedureName);
                setType(symbol.getType());
            } else {
                errorContainer.addError("Unknown procedure " + procedureName, lexer.currentLexem());
            }
            log.debug("Accepted {}", lexer.currentLexem());
        } else {
            errorContainer.addError("Expected IDENT, found: " + lexer.peek().getType(), lexer.peek());
            return false;
        }
        return true;
    }

    @Override
    public void generateCode(ClassGen cg, MethodGen mg) {
        InstructionList il = mg.getInstructionList();
        super.generateCode(cg, mg);
        il.append(new InstructionFactory(cg).createInvoke("Output", symbol.getIdentificator(), symbol.getType().javaType(), new Type[]{getChild(0).getChild(0).getType().javaType()}, Constants.INVOKESTATIC));
    }
}

