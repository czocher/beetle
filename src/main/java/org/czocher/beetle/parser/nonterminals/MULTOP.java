package org.czocher.beetle.parser.nonterminals;

import com.sun.org.apache.bcel.internal.generic.ClassGen;
import com.sun.org.apache.bcel.internal.generic.InstructionConstants;
import com.sun.org.apache.bcel.internal.generic.InstructionList;
import com.sun.org.apache.bcel.internal.generic.MethodGen;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.Lexer;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.parser.interfaces.SemanticType;
import org.czocher.beetle.parser.interfaces.SymbolScope;
import org.czocher.beetle.parser.interfaces.TreeNode;

import java.util.Arrays;
import java.util.Set;

public class MULTOP extends TreeNode {

    @Getter
    @Setter
    private String operation;

    public MULTOP(@NonNull Lexer lexer, @NonNull SymbolScope symbolScope,
                  @NonNull ErrorContainer errorContainer, @NonNull Set<LexemType> superFirstSet) {
        super(lexer, symbolScope, errorContainer, superFirstSet);
        this.firstSet.addAll(Arrays.asList(LexemType.MULTOP));
        this.followSet.addAll(Arrays.asList(LexemType.IDENT, LexemType.LSTRING, LexemType.LINT, LexemType.LBOOL,
                LexemType.LREAL, LexemType.CALL));
        this.setType(SemanticType.UNKNOWN);
    }

    @Override
    public TreeNode postParse() {
        if (lexer.peek().getType() == LexemType.MULTOP) {
            operation = lexer.<String>nextLexem().getValue();
            log.debug("Accepted {}", lexer.currentLexem());
        }
        return this;
    }

    @Override
    public void generateCode(ClassGen cg, MethodGen mg) {
        InstructionList il = mg.getInstructionList();

        super.generateCode(cg, mg);
        switch (operation) {
            case "*":
                switch (getType()) {
                    case INTEGER:
                        il.append(InstructionConstants.IMUL);
                        break;
                    case FLOAT:
                        il.append(InstructionConstants.FMUL);
                        break;
                    case BOOLEAN:
                        il.append(InstructionConstants.IMUL);
                        break;
                }
                break;
            case "/":
                switch (getType()) {
                    case INTEGER:
                        il.append(InstructionConstants.IDIV);
                        break;
                    case FLOAT:
                        il.append(InstructionConstants.FDIV);
                        break;
                    case BOOLEAN:
                        il.append(InstructionConstants.IDIV);
                        break;
                }
                break;
            case "and":
                switch (getType()) {
                    case BOOLEAN:
                        il.append(InstructionConstants.IAND);
                        break;
                    case INTEGER:
                        il.append(InstructionConstants.IAND);
                        break;
                    case FLOAT:
                        errorContainer.addError("Cannot perform and on floats!", startLine, startCol, endLine, endCol);
                        break;
                }
                break;
        }
    }
}
