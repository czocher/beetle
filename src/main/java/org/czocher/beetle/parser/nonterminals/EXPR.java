package org.czocher.beetle.parser.nonterminals;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.Lexer;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.parser.interfaces.SemanticType;
import org.czocher.beetle.parser.interfaces.SymbolScope;
import org.czocher.beetle.parser.interfaces.TreeNode;

import java.util.Arrays;
import java.util.Set;

import static org.czocher.beetle.parser.utils.Utils.sum;

public class EXPR extends TreeNode {

    @Getter
    @Setter
    private String sign;

    public EXPR(@NonNull Lexer lexer, @NonNull SymbolScope symbolScope,
                @NonNull ErrorContainer errorContainer, @NonNull Set<LexemType> superFirstSet) {
        super(lexer, symbolScope, errorContainer, superFirstSet);
        this.firstSet.addAll(Arrays.asList(LexemType.IDENT, LexemType.LSTRING, LexemType.LINT, LexemType.LBOOL,
                LexemType.LREAL, LexemType.CALL, LexemType.ADDOP));
        this.followSet.addAll(Arrays.asList(LexemType.COMMA, LexemType.CLOSING_PARENTHESIS));
        this.setType(SemanticType.UNKNOWN);
    }

    @Override
    public TreeNode postParse() {
        if (lexer.peek().getType() == LexemType.ADDOP) {
            sign = lexer.<String>nextLexem().getValue();
            if (!sign.equals("+") || !sign.equals("-")) {
                log.error("Bad EXPR symbol. Expected + or -, found: {}", sign);
                errorContainer.addError("Bad EXPR symbol. Expected + or -, found: {}", lexer.currentLexem());
                return null;
            }
        }

        TreeNode SIMPEXPR = new SIMPEXPR(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (SIMPEXPR != null) {
            addChildren(SIMPEXPR.getChildren());
        }

        TreeNode EXPR2 = new EXPR2(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (EXPR2 != null) {
            addChildren(EXPR2.getChildren());
        }

        while (children.size() > 1) {
            getChild(1).addChild(getChild(0));
            removeChild(0);
        }

        return this;
    }
}
