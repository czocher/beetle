package org.czocher.beetle.parser.nonterminals;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.Lexer;
import org.czocher.beetle.lexer.interfaces.Lexem;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.parser.interfaces.SemanticType;
import org.czocher.beetle.parser.interfaces.SymbolScope;
import org.czocher.beetle.parser.interfaces.TreeNode;

import java.util.Arrays;
import java.util.Set;

public class TYPEIDENT extends TreeNode {

    @Getter
    @Setter
    private SemanticType value;

    public TYPEIDENT(@NonNull Lexer lexer, @NonNull SymbolScope symbolScope,
                     @NonNull ErrorContainer errorContainer, @NonNull Set<LexemType> superFirstSet) {
        super(lexer, symbolScope, errorContainer, superFirstSet);
        this.firstSet.addAll(Arrays.asList(LexemType.TYPEIDENT, LexemType.ARRAYOF));
        this.followSet.addAll(Arrays.asList(LexemType.SEMICOLON));
        this.setType(SemanticType.VOID);
    }

    @Override
    public TreeNode postParse() {
        Lexem lexem = lexer.peek();
        if (lexem.getType() == LexemType.ARRAYOF) {
            TreeNode ATYPE = new ATYPE(lexer, symbolScope, errorContainer, firstSet).parse();
            switch (((TYPEIDENT) ATYPE.getChild(0)).getValue()) {
                case INTEGER:
                    value = SemanticType.ARRAY_INTEGER;
                    break;
                case FLOAT:
                    value = SemanticType.ARRAY_FLOAT;
                    break;
                case BOOLEAN:
                    value = SemanticType.ARRAY_BOOLEAN;
                    break;
                case STRING:
                    value = SemanticType.ARRAY_STRING;
                    break;
            }

        } else {
            SemanticType val = SemanticType.fromTYPEIDENTLexem(lexer.<String>nextLexem());
            if (val != null) {
                value = val;
                log.debug("Accepted {}", lexer.currentLexem());
            } else {
                return null;
            }
            log.debug("Found " + value);
        }


        return this;
    }
}
