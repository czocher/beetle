package org.czocher.beetle.parser.nonterminals;

import com.sun.org.apache.bcel.internal.Constants;
import com.sun.org.apache.bcel.internal.generic.*;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.Lexer;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.parser.interfaces.SemanticType;
import org.czocher.beetle.parser.interfaces.Symbol;
import org.czocher.beetle.parser.interfaces.SymbolScope;
import org.czocher.beetle.parser.interfaces.TreeNode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.czocher.beetle.parser.utils.Utils.getLocalVariableByName;
import static org.czocher.beetle.parser.utils.Utils.sum;

public class PROCDECL extends TreeNode {

    @Getter
    @Setter
    private Symbol symbol;

    public PROCDECL(@NonNull Lexer lexer, @NonNull SymbolScope symbolScope,
                    @NonNull ErrorContainer errorContainer, @NonNull Set<LexemType> superFirstSet) {
        super(lexer, symbolScope, errorContainer, superFirstSet);
        this.firstSet.addAll(Arrays.asList(LexemType.SUB));
        this.followSet.addAll(Arrays.asList(LexemType.MAIN));
        this.setType(SemanticType.VOID);
    }

    @Override
    public TreeNode postParse() {
        acceptOrSkip(LexemType.SUB);

        String procName = parseIdent();
        if (procName == null) {
            return null;
        }
        symbol = new Symbol(procName, SemanticType.UNKNOWN, false);
        symbolScope.addSymbol(symbol);
        symbolScope = symbolScope.subScope();

        parseFPARAMS();
        parseTYPEIDENT();
        symbol.setType(type);


        acceptOrSkip(LexemType.SEMICOLON);
        parseBody();
        return this;
    }

    private String parseIdent() {
        String procName;
        if (lexer.peek().getType() == LexemType.IDENT) {
            procName = lexer.<String>nextLexem().getValue();
            log.debug("Accepted {}", lexer.currentLexem());
        } else {
            errorContainer.addError("Expected IDENT, found " + lexer.peek(), lexer.peek());
            return null;
        }
        return procName;
    }

    private void parseFPARAMS() {
        if (lexer.peek().getType() != LexemType.OPENING_PARENTHESIS) {
            return;
        }
        acceptOrSkip(LexemType.OPENING_PARENTHESIS);
        TreeNode FPARAMS = new FPARAMS(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (FPARAMS != null) {
            addChild(FPARAMS);
        }
        acceptOrSkip(LexemType.CLOSING_PARENTHESIS);
    }

    private void parseTYPEIDENT() {
        if (lexer.peek().getType() != LexemType.COLON) {
            return;
        }
        acceptOrSkip(LexemType.COLON);
        TYPEIDENT TYPEIDENT = (TYPEIDENT) new TYPEIDENT(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (TYPEIDENT != null) {
            type = TYPEIDENT.getValue();
        }
    }

    private void parseBody() {
        acceptOrSkip(LexemType.OPENING_BRACE);
        TreeNode VARSEQ = new VARSEQ(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (VARSEQ != null) {
            addChild(VARSEQ);
        }
        TreeNode INSTRSEQ = new INSTRSEQ(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (INSTRSEQ != null) {
            addChild(INSTRSEQ);
        }
        acceptOrSkip(LexemType.CLOSING_BRACE);
    }

    @Override
    public void generateCode(ClassGen cg, MethodGen mg) {
        InstructionList instructionList = new InstructionList();
        List<Type> argTypes = new ArrayList<>();
        List<String> argNames = new ArrayList<>();

        int i = 0;
        for (TreeNode t : getChild(0).getChildren()) {
            argNames.add(((FPARAM) t).getSymbol().getIdentificator());
            argTypes.add(((FPARAM) t).getSymbol().getType().javaType());
            ((FPARAM) t).getSymbol().setIndex(i++);
        }

        MethodGen mg2 = new MethodGen(Constants.ACC_PUBLIC
                | Constants.ACC_STATIC, symbol.getType().javaType(), argTypes.toArray(new Type[argTypes.size()]), argNames.toArray(new String[argNames.size()]), symbol.getIdentificator(), "Output",
                instructionList, cg.getConstantPool());
        mg2.addLocalVariable(symbol.getIdentificator(), symbol.getType().javaType(), null, null);
        super.generateCode(cg, mg2);

        instructionList.append(InstructionFactory.createLoad(symbol.getType().javaType(), getLocalVariableByName(mg2.getLocalVariables(), symbol.getIdentificator()).getIndex()));
        switch (symbol.getType()) {
            case ARRAY_INTEGER:
            case ARRAY_FLOAT:
            case ARRAY_STRING:
            case ARRAY_BOOLEAN:
            case STRING:
                instructionList.append(InstructionConstants.ARETURN);
                break;
            case INTEGER:
            case BOOLEAN:
                instructionList.append(InstructionConstants.IRETURN);
                break;
            case FLOAT:
                instructionList.append(InstructionConstants.FRETURN);
                break;
            case VOID:
                instructionList.append(InstructionConstants.RETURN);
                break;
        }
        mg2.setMaxStack();
        mg2.setMaxStack();
        cg.addMethod(mg2.getMethod());
    }
}
