package org.czocher.beetle.parser.nonterminals;

import lombok.NonNull;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.Lexer;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.parser.interfaces.SemanticType;
import org.czocher.beetle.parser.interfaces.SymbolScope;
import org.czocher.beetle.parser.interfaces.TreeNode;

import java.util.Arrays;
import java.util.Set;

import static org.czocher.beetle.parser.utils.Utils.sum;

public class APARAMS extends TreeNode {

    public APARAMS(@NonNull Lexer lexer, @NonNull SymbolScope symbolScope,
                   @NonNull ErrorContainer errorContainer, @NonNull Set<LexemType> superFirstSet) {
        super(lexer, symbolScope, errorContainer, superFirstSet);
        this.firstSet.addAll(Arrays.asList(LexemType.IDENT, LexemType.LSTRING, LexemType.LINT, LexemType.LBOOL,
                LexemType.LREAL, LexemType.CALL, LexemType.ADDOP));
        this.followSet.addAll(Arrays.asList(LexemType.CLOSING_PARENTHESIS));
        this.setType(SemanticType.VOID);
    }

    @Override
    public TreeNode postParse() {
        TreeNode EXPR = new EXPR(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (EXPR != null) {
            addChildren(EXPR.getChildren());
        } else {
            errorContainer.addError("Expected EXPR.", lexer.peek());
            return null;
        }

        TreeNode APARAMS2 = new APARAMS2(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (APARAMS2 != null) {
            addChild(APARAMS2);
        }

        return this;
    }
}
