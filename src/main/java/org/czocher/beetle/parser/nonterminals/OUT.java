package org.czocher.beetle.parser.nonterminals;

import com.sun.org.apache.bcel.internal.Constants;
import com.sun.org.apache.bcel.internal.generic.*;
import lombok.NonNull;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.Lexer;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.parser.interfaces.SemanticType;
import org.czocher.beetle.parser.interfaces.SymbolScope;
import org.czocher.beetle.parser.interfaces.TreeNode;

import java.util.Arrays;
import java.util.Set;

import static org.czocher.beetle.parser.utils.Utils.sum;

public class OUT extends TreeNode {

    public OUT(@NonNull Lexer lexer, @NonNull SymbolScope symbolScope,
               @NonNull ErrorContainer errorContainer, @NonNull Set<LexemType> superFirstSet) {
        super(lexer, symbolScope, errorContainer, superFirstSet);
        this.firstSet.addAll(Arrays.asList(LexemType.PRINT));
        this.followSet.addAll(Arrays.asList(LexemType.SEMICOLON));
        this.setType(SemanticType.VOID);
    }

    @Override
    public TreeNode postParse() {
        acceptOrSkip(LexemType.PRINT);
        acceptOrSkip(LexemType.OPENING_PARENTHESIS);

        TreeNode EXPR = new EXPR(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (EXPR != null) {
            addChildren(EXPR.getChildren());
        } else {
            errorContainer.addError("Expected EXPR.", lexer.currentLexem());
            return null;
        }

        acceptOrSkip(LexemType.CLOSING_PARENTHESIS);

        return this;
    }

    @Override
    public void generateCode(ClassGen cg, MethodGen mg) {
        InstructionList il = mg.getInstructionList();

        InstructionFactory f = new InstructionFactory(cg);
        il.append(f.createFieldAccess("java.lang.System", "out",
                new ObjectType("java.io.PrintStream"), Constants.GETSTATIC));
        super.generateCode(cg, mg);

        SemanticType childType = getChild(0).getType();

        il.append(f.createInvoke("java.io.PrintStream", "println", Type.VOID,
                new Type[]{childType.javaSubType()}, Constants.INVOKEVIRTUAL));
    }
}
