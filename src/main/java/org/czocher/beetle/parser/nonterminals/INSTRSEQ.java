package org.czocher.beetle.parser.nonterminals;

import lombok.NonNull;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.Lexer;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.parser.interfaces.SemanticType;
import org.czocher.beetle.parser.interfaces.SymbolScope;
import org.czocher.beetle.parser.interfaces.TreeNode;

import java.util.Arrays;
import java.util.Set;

import static org.czocher.beetle.parser.utils.Utils.sum;

public class INSTRSEQ extends TreeNode {

    public INSTRSEQ(@NonNull Lexer lexer, @NonNull SymbolScope symbolScope,
                    @NonNull ErrorContainer errorContainer, @NonNull Set<LexemType> superFirstSet) {
        super(lexer, symbolScope, errorContainer, superFirstSet);
        this.firstSet.addAll(Arrays.asList(LexemType.IDENT, LexemType.IF, LexemType.DO,
                LexemType.READ, LexemType.PRINT, LexemType.BLOCK, LexemType.CALL, LexemType.ALLOC));
        this.followSet.addAll(Arrays.asList(LexemType.CLOSING_BRACE));
        this.setType(SemanticType.VOID);
    }

    @Override
    public TreeNode postParse() {
        TreeNode INSTR = new INSTR(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (INSTR != null) {
            addChildren(INSTR.getChildren());
        }

        acceptOrSkip(LexemType.SEMICOLON);

        TreeNode INSTRSEQ = new INSTRSEQ(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (INSTRSEQ != null) {
            addChildren(INSTRSEQ.getChildren());
        }

        return this;
    }
}
