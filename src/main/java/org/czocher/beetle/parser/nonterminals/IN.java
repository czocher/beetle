package org.czocher.beetle.parser.nonterminals;

import com.sun.org.apache.bcel.internal.Constants;
import com.sun.org.apache.bcel.internal.generic.*;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.Lexer;
import org.czocher.beetle.lexer.interfaces.Lexem;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.parser.interfaces.SemanticType;
import org.czocher.beetle.parser.interfaces.Symbol;
import org.czocher.beetle.parser.interfaces.SymbolScope;
import org.czocher.beetle.parser.interfaces.TreeNode;

import java.util.Arrays;
import java.util.Set;

import static org.czocher.beetle.parser.utils.Utils.getLocalVariableByName;

public class IN extends TreeNode {

    @Getter
    @Setter
    private Symbol symbol;

    public IN(@NonNull Lexer lexer, @NonNull SymbolScope symbolScope,
              @NonNull ErrorContainer errorContainer, @NonNull Set<LexemType> superFirstSet) {
        super(lexer, symbolScope, errorContainer, superFirstSet);
        this.firstSet.addAll(Arrays.asList(LexemType.READ));
        this.followSet.addAll(Arrays.asList(LexemType.SEMICOLON));
        this.setType(SemanticType.VOID);
    }

    @Override
    public TreeNode postParse() {
        acceptOrSkip(LexemType.READ);
        acceptOrSkip(LexemType.OPENING_PARENTHESIS);

        Lexem ident = lexer.nextLexem();

        if (ident.getType() == LexemType.IDENT) {
            String varName = (String) ident.getValue();
            if (symbolScope.variableExists(varName)) {
                symbol = symbolScope.getSymbol(varName);
            } else {
                errorContainer.addError("Unknown variable " + varName, ident);
            }
            log.debug("Accepted {}", ident);
        } else {
            errorContainer.addError("Expected IDENT, found: " + ident.getType(), ident);
            return null;
        }

        acceptOrSkip(LexemType.CLOSING_PARENTHESIS);

        return this;
    }

    @Override
    public void generateCode(ClassGen cg, MethodGen mg) {
        InstructionList il = mg.getInstructionList();

        InstructionFactory f = new InstructionFactory(cg);

        super.generateCode(cg, mg);

        il.append(f.createInvoke("java.lang.System", "console", new ObjectType("java.io.Console"),
                new Type[]{}, Constants.INVOKESTATIC));
        il.append(f.createInvoke("java.io.Console", "readLine", new ObjectType("java.lang.String"),
                new Type[]{}, Constants.INVOKEVIRTUAL));


        SemanticType childType = symbol.getType();

        switch (childType) {
            case BOOLEAN:
                il.append(f.createInvoke("java.lang.Boolean", "parseBool", Type.BOOLEAN,
                        new Type[]{new ObjectType("java.lang.String")}, Constants.INVOKESTATIC));
                break;
            case INTEGER:
                il.append(f.createInvoke("java.lang.Integer", "parseInt", Type.INT,
                        new Type[]{new ObjectType("java.lang.String")}, Constants.INVOKESTATIC));
                break;
            case FLOAT:
                il.append(f.createInvoke("java.lang.Float", "parseFloat", Type.FLOAT,
                        new Type[]{new ObjectType("java.lang.String")}, Constants.INVOKESTATIC));
                break;
        }

        if (symbolScope.isLocal(symbol.getIdentificator())) {
            System.out.println("Local: " + symbol.getIdentificator());
            il.append(new InstructionFactory(cg).createStore(symbol.getType().javaType(), getLocalVariableByName(mg.getLocalVariables(), symbol.getIdentificator()).getIndex()));
        } else {
            il.append(new InstructionFactory(cg).createPutStatic("Output", symbol.getIdentificator(), symbol.getType().javaType()));
        }


    }
}
