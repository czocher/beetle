package org.czocher.beetle.parser.nonterminals;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.Lexer;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.parser.interfaces.SemanticType;
import org.czocher.beetle.parser.interfaces.SymbolScope;
import org.czocher.beetle.parser.interfaces.TreeNode;

import java.util.Arrays;
import java.util.Set;

public class RELOP extends TreeNode {

    @Getter
    @Setter
    private String operation;

    public RELOP(@NonNull Lexer lexer, @NonNull SymbolScope symbolScope,
                 @NonNull ErrorContainer errorContainer, @NonNull Set<LexemType> superFirstSet) {
        super(lexer, symbolScope, errorContainer, superFirstSet);
        this.firstSet.addAll(Arrays.asList(LexemType.RELOP));
        this.followSet.addAll(Arrays.asList(LexemType.IDENT, LexemType.LSTRING, LexemType.LINT, LexemType.LBOOL,
                LexemType.LREAL, LexemType.CALL));
        this.setType(SemanticType.BOOLEAN);
    }

    @Override
    public TreeNode postParse() {
        if (lexer.peek().getType() == LexemType.RELOP) {
            operation = lexer.<String>nextLexem().getValue();
            log.debug("Accepted {}", lexer.currentLexem());
        }
        return this;
    }
}
