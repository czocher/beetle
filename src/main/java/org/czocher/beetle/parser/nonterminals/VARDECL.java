package org.czocher.beetle.parser.nonterminals;

import com.sun.org.apache.bcel.internal.Constants;
import com.sun.org.apache.bcel.internal.generic.ClassGen;
import com.sun.org.apache.bcel.internal.generic.FieldGen;
import com.sun.org.apache.bcel.internal.generic.MethodGen;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.Lexer;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.parser.interfaces.SemanticType;
import org.czocher.beetle.parser.interfaces.Symbol;
import org.czocher.beetle.parser.interfaces.SymbolScope;
import org.czocher.beetle.parser.interfaces.TreeNode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static org.czocher.beetle.parser.utils.Utils.sum;

public class VARDECL extends TreeNode {

    @Getter
    @Setter
    private List<Symbol> symbols;

    public VARDECL(@NonNull Lexer lexer, @NonNull SymbolScope symbolScope,
                   @NonNull ErrorContainer errorContainer, @NonNull Set<LexemType> superFirstSet) {
        super(lexer, symbolScope, errorContainer, superFirstSet);
        this.firstSet.addAll(Arrays.asList(LexemType.VAR));
        this.followSet.addAll(Arrays.asList(LexemType.SEMICOLON));
        this.setType(SemanticType.VOID);
        this.symbols = new ArrayList<>();
    }

    @Override
    public TreeNode postParse() {
        List<String> variableNames = new ArrayList<>();
        SemanticType variableType = SemanticType.UNKNOWN;
        acceptOrSkip(LexemType.VAR);

        IDLIST IDLIST = (IDLIST) new IDLIST(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (IDLIST != null) {
            variableNames.addAll(IDLIST.getValue());
        }

        acceptOrSkip(LexemType.COLON);

        TYPEIDENT TYPEIDENT = (TYPEIDENT) new TYPEIDENT(lexer, symbolScope,
                errorContainer, sum(firstSet, superFirstSet)).parse();
        if (TYPEIDENT != null) {
            variableType = TYPEIDENT.getValue();
        }

        for (String variableName : variableNames) {
            Symbol v = new Symbol(variableName, variableType, true);
            symbols.add(v);
            symbolScope.addSymbol(v);
            type = v.getType();
        }

        return this;
    }

    @Override
    public void generateCode(ClassGen cg, MethodGen mg) {
        if (mg != null) {
            // We're in a method or main
            for (Symbol s : symbols) {
                mg.addLocalVariable(s.getIdentificator(), s.getType().javaType(), null, null);
            }
        } else {
            // We're in the global score
            for (Symbol s : symbols) {
                cg.addField(new FieldGen(Constants.ACC_PUBLIC | Constants.ACC_STATIC, s.getType().javaType(), s.getIdentificator(), cg.getConstantPool()).getField());
            }
        }
        super.generateCode(cg, mg);
    }
}
