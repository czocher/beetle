package org.czocher.beetle.parser.nonterminals;

import lombok.NonNull;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.Lexer;
import org.czocher.beetle.lexer.interfaces.LexemType;
import org.czocher.beetle.parser.interfaces.SemanticType;
import org.czocher.beetle.parser.interfaces.SymbolScope;
import org.czocher.beetle.parser.interfaces.TreeNode;

import java.util.Arrays;
import java.util.Set;

import static org.czocher.beetle.parser.utils.Utils.sum;

public class TERM2 extends TreeNode {

    public TERM2(@NonNull Lexer lexer, @NonNull SymbolScope symbolScope,
                 @NonNull ErrorContainer errorContainer, @NonNull Set<LexemType> superFirstSet) {
        super(lexer, symbolScope, errorContainer, superFirstSet);
        this.firstSet.addAll(Arrays.asList(LexemType.MULTOP));
        this.followSet.addAll(Arrays.asList(LexemType.ADDOP));
        this.setType(SemanticType.UNKNOWN);
    }

    @Override
    public TreeNode postParse() {
        TreeNode MULTOP = new MULTOP(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        addChild(MULTOP);

        TreeNode FACTOR = new FACTOR(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (FACTOR != null) {
            MULTOP.addChild(0, FACTOR);
        }

        TreeNode TERM2 = new TERM2(lexer, symbolScope, errorContainer, sum(firstSet, superFirstSet)).parse();
        if (TERM2 != null) {
            addChildren(TERM2.getChildren());
        }
        return this;
    }
}
