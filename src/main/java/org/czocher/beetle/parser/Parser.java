package org.czocher.beetle.parser;

import lombok.Data;
import lombok.NonNull;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.Lexer;
import org.czocher.beetle.parser.interfaces.SymbolScope;
import org.czocher.beetle.parser.interfaces.TreeNode;
import org.czocher.beetle.parser.nonterminals.PROGRAM;

@Data
public class Parser {

    private final SymbolScope symbolScope;
    private final ErrorContainer errorContainer;
    private final Lexer lexer;

    public Parser(@NonNull Lexer lexer, @NonNull ErrorContainer errorContainer) {
        this.symbolScope = new SymbolScope();
        this.lexer = lexer;
        this.errorContainer = errorContainer;
    }

    public TreeNode parse() {
        return new PROGRAM(lexer, symbolScope, errorContainer).parse();
    }

}
