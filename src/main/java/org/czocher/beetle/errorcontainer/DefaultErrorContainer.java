package org.czocher.beetle.errorcontainer;

import lombok.ToString;
import org.czocher.beetle.errorcontainer.interfaces.Error;
import org.czocher.beetle.errorcontainer.interfaces.ErrorContainer;
import org.czocher.beetle.lexer.interfaces.Lexem;

import java.util.ArrayList;
import java.util.List;

@ToString
public class DefaultErrorContainer implements ErrorContainer {

    private final List<Error> errorList;

    public DefaultErrorContainer() {
        errorList = new ArrayList<>();
    }

    @Override
    public void addError(final String message, final int startLine, final int startCol, final int endLine, final int endCol) {
        this.addError(new Error(message, startLine, startCol, endLine, endCol));
    }

    @Override
    public void addError(String message, Lexem lexem) {
        this.addError(message, lexem.getStartLine(), lexem.getStartCol(), lexem.getEndLine(), lexem.getEndCol());
    }

    @Override
    public void addError(final Error error) {
        this.errorList.add(error);
    }

    @Override
    public List<Error> getErrorList() {
        return errorList;
    }
}
