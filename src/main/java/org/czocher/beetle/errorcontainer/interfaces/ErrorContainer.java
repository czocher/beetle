package org.czocher.beetle.errorcontainer.interfaces;

import org.czocher.beetle.lexer.interfaces.Lexem;

import java.util.List;

public interface ErrorContainer {

    public void addError(final String message, final int startLine, final int startCol, final int endLine, final int endCol);

    public void addError(final String message, final Lexem lexem);

    public void addError(final Error error);

    public List<Error> getErrorList();

}
