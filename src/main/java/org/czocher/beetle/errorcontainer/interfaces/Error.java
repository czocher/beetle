package org.czocher.beetle.errorcontainer.interfaces;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Error {

    private String message;
    private int startLine;
    private int startCol;
    private int endLine;
    private int endCol;

}
